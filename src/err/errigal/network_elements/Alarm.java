package err.errigal.network_elements;
import static err.errigal.network_elements.AlarmType.Status;
import static err.errigal.network_elements.AlarmType.Status.CLEARED;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;


/*
 *  Purpose: Alarm - serves as alarms that can be added to net elements, immutable.
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public final class Alarm implements Comparable<Alarm> {

    private final int id;
    private final LocalDateTime createdDate; //LDT is immutable
    private final LocalDateTime clearDate;   //LDT is immutable
    private final AlarmType alarmType;
    private final Status currentAlarmStatus;
    private final Status previousAlarmStatus;
    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    private static final Set<Integer> alarmId;

    static {
        alarmId = new HashSet<>();
    }

    private Alarm(AlarmBuilder builder) {
        id = builder.id;
        createdDate = builder.createdDate;
        alarmType = builder.alarmType;
        currentAlarmStatus = builder.currentAlarmStatus;
        clearDate = builder.clearDate;
        previousAlarmStatus = builder.previousAlarmStatus;
    }

    public static final class AlarmBuilder {

        private final int id;
        private final LocalDateTime createdDate;
        private final AlarmType alarmType;
        private Status currentAlarmStatus = null;
        private Status previousAlarmStatus = null;
        private LocalDateTime clearDate = null;

        public AlarmBuilder(int id, LocalDateTime createdDate, AlarmType alarmType) {
            this.id = id;
            this.createdDate = createdDate;
            this.alarmType = alarmType;
            alarmId.add(id);
        }

        public AlarmBuilder(Alarm alarm) {
            this.id = alarm.id;
            this.createdDate = alarm.createdDate;
            this.clearDate = alarm.clearDate;
            this.alarmType = alarm.alarmType;
            this.clearDate = alarm.clearDate;
            this.currentAlarmStatus = alarm.currentAlarmStatus;
            this.previousAlarmStatus = alarm.previousAlarmStatus;
        }

        public AlarmBuilder clearDate(LocalDateTime clearDate) {
            this.clearDate = clearDate;
            return this;
        }


        public AlarmBuilder currentStatus(Status status) {
            this.currentAlarmStatus = status;
            return this;
        }

        public AlarmBuilder previousStatus(Status status) {
            this.previousAlarmStatus = status;
            return this;
        }

        public Alarm build() {
            return new Alarm(this);
        }
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public LocalDateTime getClearDate() {
        return clearDate;
    }

    public AlarmType getAlarmType() {
        return alarmType;
    }

    public Status getCurrentStatus() {
        return currentAlarmStatus;
    }

    public Status getPreviousStatus() {
        return previousAlarmStatus;
    }

    public String getRemedy() {
        if(currentAlarmStatus != CLEARED)
            return alarmType.getRemedy();
        return "ALARM CLEARED";
    }

    @Override
    public String toString() {

        String clearDateString = "PENDING";

        if(clearDate != null)
            clearDateString = df.format(clearDate);

        return "ALARM ID: "+id+", ALARM TYPE: "+ alarmType +", CREATED: "+df.format(createdDate)+", "+
                " CLEAR_DATE: "+clearDateString+", CURRENT STATUS: "+currentAlarmStatus+", PREVIOUS STATUS: "+previousAlarmStatus;
    }

    @Override
    public int hashCode() {
        int val = 23;
        val = ((val << 5) - val) + id;
        val = ((val << 5) - val) + ((createdDate == null)? 0 : createdDate.hashCode());
        val = ((val << 5) - val) + ((clearDate == null)? 0 : clearDate.hashCode());
        val = ((val << 5) - val) + ((alarmType == null)? 0 : alarmType.hashCode());
        val = ((val << 5) - val) + ((previousAlarmStatus == null)? 0 : previousAlarmStatus.hashCode());
        val = ((val << 5) - val) + ((previousAlarmStatus == null)? 0 : previousAlarmStatus.hashCode());

        return val;
    }


    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!(o instanceof Alarm))
            return false;

        Alarm a = (Alarm)o;

        if(id != a.id)
            return false;
        if((createdDate == null)?(a.clearDate!=null): !createdDate.equals(a.createdDate))
            return false;
        if((clearDate == null)?(a.clearDate!=null): !clearDate.equals(a.clearDate))
            return false;
        if(alarmType != a.alarmType)
            return false;
        if(currentAlarmStatus != a.currentAlarmStatus)
            return false;
        if(previousAlarmStatus != a.previousAlarmStatus)
            return false;
        return true;
    }

    public static boolean alarmIDExists(int id) {
        return alarmId.contains(id);
    }

    public int compareTo(Alarm o) {

         if(currentAlarmStatus.getPriorityLevel() == o.currentAlarmStatus.getPriorityLevel()) {
             return 0;
         }
         else if(currentAlarmStatus.getPriorityLevel() > o.currentAlarmStatus.getPriorityLevel()) {
             return -1; //higher priority sorted to top
         }

         return 1;
    }
}



