package err.errigal.network_elements;
import err.errigal.network_elements.Node.NodeBuilder;
import static err.errigal.network_elements.AlarmType.*;
import static err.errigal.network_elements.AlarmType.Status.CLEARED;
import java.util.*;

/*
 *  Purpose: Network Element - Instances of this class, represent a hub on a network, can have child nodes
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public final class Hub extends NetworkElement {

    private Map<String, Node> nodes;

    protected Hub (HubBuilder builder) {
        super(builder);
        nodes = builder.nodesByName;
    }

    public static final class HubBuilder extends NetBuilder<HubBuilder, Hub>  {
        private Map<String, Node> nodesByName;

        public HubBuilder(int id, String name) {
            super(id, NeType.HUB, name);
            this.nodesByName = new LinkedHashMap<>();
        }

        /* Utility Copy cons */
        public HubBuilder(Hub hub) {
            super(hub);
            this.nodesByName = new LinkedHashMap<>();
            for(Node node : hub.getNodes()) {
                nodesByName.put(node.getName(), new NodeBuilder(node).build());
            }
        }

        public HubBuilder(Hub hub, String name) {
            super(hub, name);
            this.nodesByName = hub.nodes;
        }

        public Hub build() {
            return new Hub(this);
        }
    }

    public void displayChildren() {
        for(Node node : nodes.values())
            System.out.println("\t\t\t->" + node);
    }

    public List<Node> getNodes() {
        List<Node> copyNodeList = new ArrayList<>();
        nodes.forEach((String K, Node V) ->
                copyNodeList.add(V));
        return copyNodeList;
    }

    protected Node getNodeByName(String name) {
        if(!nodeExists(name))
            return null;
        return nodes.get(name);
    }

    public int getChildCount() {
        return nodes.size();
    }

    protected boolean addNode(Node child) {
        if(child == null || nodes.containsKey(child.getName()))
            return false;

        child.setParentId(this.getID());
        addObserver(child);
        nodes.put(child.getName(), child);
        return true;
    }


    @Override
    public final boolean clearAlarm(Alarm alarm) {
        boolean res = super.clearAlarm(alarm);

        if(!res) {
            return false;
        }

        updateObservers();
        return true;
    }

    protected boolean removeNode(Node node) {

        if(!nodeExists(node.getName()))
            return false;

        boolean res = nodes.remove(node.getName(), node);

        if(res) removeObserver(node);

        return res;
    }

    protected boolean removeNodeByName(String name) {
        if(!nodeExists(name))
            return false;
        removeObserver(nodes.get(name));
        return nodes.remove(name, nodes.get(name));
    }

    public boolean nodeExists(String nodeName) {
        return nodes.containsKey(nodeName);
    }

    public void changeNodeName(Node node, String name) {
        Node changeNode = new NodeBuilder(node, name).build();
        nodes.remove(node.getName(), node);
        nodes.put(changeNode.getName(), changeNode);
    }

    @Override
    public int hashCode() {
        int superHash = super.hashCode();
        superHash = ((superHash<<5) - superHash)+((nodes == null)?0: nodes.hashCode());
        return superHash;
    }

    public void setNetworkNodes(List<Node> nodeList) {
        this.nodes = new LinkedHashMap<>();

        for(Node n : nodeList)
            this.nodes.put(n.getName(), n);
    }
    
    @Override
    public boolean equals(Object o) {
        if(!super.equals(o))
            return false;
        if(!(o instanceof Hub))
            return false;
        Hub hub = (Hub)o;
        return (nodes.equals(hub.nodes));
    }
}
