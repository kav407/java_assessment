package err.errigal.network_elements;
import err.errigal.network_elements.AlarmType.Status;
import static err.errigal.network_elements.AlarmType.UNIT_UNAVAILABLE;


/*
 *  Purpose: Network Element Node -  Represents a node on that can be added to the app network
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */


public class Node extends NetworkElement {


    public Node(NodeBuilder builder) {
        super(builder);
    }

    public static final class NodeBuilder extends NetworkElement.NetBuilder<NodeBuilder, Node> {

        public NodeBuilder(int id, String name) {
            super(id, NeType.NODE, name);
        }

        public NodeBuilder(Node copyNode) {
            super(copyNode);
        }
        public NodeBuilder(Node copyNode, String name) {
            super(copyNode, name);
        }

        public Node build() {
            return new Node(this);
        }

    }

    @Override
    public void update(NetworkElementState state) {
         for(Alarm a : getAlarmList())
             if(a.getAlarmType() == UNIT_UNAVAILABLE && a.getCurrentStatus() != Status.CLEARED)
                 return;
            this.currentState = state;
    }
}
