package err.errigal.network_elements;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.*;
import static err.errigal.network_elements.Hub.*;
import static err.errigal.network_elements.NetworkElement.NetworkElementState.*;


/*
 *  Purpose: Network Carrier - Instances of this class, represent carriers on a network, can have child hubs/nodes
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class Carrier {

    private final int id;
    private final String name;
    private Map<String, Hub> hubs;


    public Carrier(int id, String name) {
        this.id = id;
        this.name = name;
        hubs = new LinkedHashMap<>();
    }

    //Deep Copy Cons
    public Carrier(Carrier carrier) {
        this.id = carrier.id;
        this.name = carrier.name;
        this.hubs = new LinkedHashMap<>();

        for(Map.Entry<String,Hub> entry : carrier.hubs.entrySet()) {
            Hub hub = new HubBuilder(entry.getValue()).build();
            this.hubs.put(hub.getName(), hub);
        }
    }

    //utility shallow copy cons
    public Carrier(String name, Carrier carrier) {
        this.id = carrier.id;
        this.name = name;
        this.hubs = carrier.hubs;

    }

    protected boolean addHub(Hub child) {
        if(child == null || hubExists(child.getName()))
            return false;

        child.setParentId(id);
        hubs.put(child.getName(), child);
        return true;
    }

    protected boolean removeHub(Hub hub) {
        if(!hubExists(hub.getName()))
            return false;
        Hub ret = hubs.remove(hub.getName());
        if(ret == null)
            return false;
        return true;
    }

    protected boolean removeHubByName(String hubName) {
        if(!hubExists(hubName))
            return false;
        Hub ret = hubs.remove(hubName);
        if(ret == null)
            return false;
        return true;
    }

    public void displayChildren() {
        for(Hub hub : hubs.values())
            System.out.println("\t\t-> Hub " + hub);

    }

    public List<Hub> getHubList() {
        List<Hub> copyListHubs = new ArrayList<>();
        hubs.forEach((String k, Hub v)-> copyListHubs.add(v));
        return copyListHubs;
    }

    public Map<String, Hub> getHubs() {
        return hubs;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getChildCount() {
        return hubs.size();
    }

    public int getGrandChildCount() {
        int count = 0;
        for(Hub hub : hubs.values())
            count+=hub.getChildCount();
        return count;
    }

    public Hub getHubByName(String name) {
        return hubs.get(name);
    }

    public boolean hubExists(String name) {
        if(name == null) {
            return false;
        }

        return hubs.containsKey(name);
    }

    public void changeHubName(Hub currentHub, String name) {
        Hub newHub = new HubBuilder(currentHub, name).build();
        hubs.remove(currentHub.getName(), currentHub);
        hubs.put(newHub.getName(), newHub);
    }

    public String toString() {
        return name+", ID: "+id+", Net Child Elements: "+getChildCount()+", Net GrandChild Elements: "+getGrandChildCount();
    }


}
