package err.errigal.network_elements;
import err.errigal.network_elements.Alarm.AlarmBuilder;
import err.errigal.patternInterfaces.Observable;
import err.errigal.patternInterfaces.Observer;
import java.time.LocalDateTime;
import java.util.*;
import static err.errigal.network_elements.AlarmType.*;
import static err.errigal.network_elements.AlarmType.Status.*;
import static err.errigal.network_elements.NetworkElement.NetworkElementState.*;


/*
 *  Purpose: Network Element Abstraction -  Provides base class for all network elements, providing default and enforcing required behaviour
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public abstract class NetworkElement implements Observable, Observer, Comparable<NetworkElement> {

    private final int id;
    private final NeType type;
    private final String name;
    private String ipAddress;
    private int parentId;
    private String description;
    private Map<String, List<Alarm>> alarms;
    protected NetworkElementState currentState;
    private transient List<Observer> netElementObservers; //this will have to be rebuilt after deserialization

    public enum NetworkElementState {
        OFFLINE("OFFLINE", 0), ONLINE("ONLINE", 1);

        private String name;
        private int index;

        NetworkElementState(String name, int index) {
            this.name = name;
            this.index = index;
        }
        public String toString() {
            return name;
        }
    }

    NetworkElement (NetBuilder builder) {
        this.id = builder.id;
        this.type = builder.type;
        this.name = builder.name;
        this.ipAddress = builder.ipAddress;
        this.parentId = builder.parentId;
        this.description = builder.description;
        this.alarms = builder.alarms;
        this.currentState = builder.state;
        this.netElementObservers = builder.observers;
    }

    public final int getID() {
        return id;
    }

    public final int getParentID() {
        return parentId;
    }

    public final String getIP() {
        return ipAddress;
    }

    public final String getDescription() {
        return description;
    }

    public final String getName() {
        return name;
    }

    public final NeType getType() {
        return type;
    }

    public final int getAlarmCount() {
        int count = 0;
        for(List<Alarm> a : alarms.values())
            count += a.size();
        return count;
    }

    public final Map<String, List<Alarm>> getAlarms() {
        Map<String,List<Alarm>> copyAlarmMap = new LinkedHashMap<>();

        for(Map.Entry<String, List<Alarm>> entry : alarms.entrySet()) {
            List<Alarm> temp = new ArrayList<>();
            copyAlarmMap.put(entry.getKey(), temp);
            entry.getValue().forEach((alarm) ->  temp.add(alarm));
        }

        return copyAlarmMap;
    }

    public final List<Alarm> getAlarmByType(AlarmType type) {
        if(type == null) {
            return null;
        }
        if(alarms.get(type.toString()) == null || !alarms.containsKey(type.toString())) {
            return null;
        }

        List<Alarm> copyAlarms = new ArrayList<>();
        List<Alarm> temp = alarms.get(type.toString());

        temp.forEach((alarm) ->
          copyAlarms.add(new AlarmBuilder(alarm).build())
        );

        return copyAlarms;
    }

    public final List<Alarm> getAlarmList() {
        ArrayList<Alarm> copyAlarms = new ArrayList<>();

        for(Map.Entry<String, List<Alarm>> entry : alarms.entrySet()) {
            List<Alarm> temp = entry.getValue();
            temp.forEach((alarm)-> copyAlarms.addAll(temp));
        }
        return copyAlarms;
    }

    public boolean addAlarm(Alarm alarm) {
        if(alarm == null) {
            return false;
        }
        if(alarms.get(alarm.getAlarmType().toString()) == null) {
            alarms.put(alarm.getAlarmType().toString(), new ArrayList<>());
        }
        for(List<Alarm> list : alarms.values()) {
            if (list.contains(alarm)) {
                return false;
            }
        }

        alarms.get(alarm.getAlarmType().toString()).add(alarm);

        if(alarm.getAlarmType() == UNIT_UNAVAILABLE) {
            currentState = OFFLINE;
        }

        updateObservers(); //Update node children, of a status change in parent

        return true;
    }

    protected boolean clearAlarm(Alarm alarm) {

        if(alarm == null || alarms.get(alarm.getAlarmType().toString()) == null) {
            return false;
        }

        for(List<Alarm> list : alarms.values()) {
            if (!list.contains(alarm)) {
                return false;
            }
        }

        AlarmBuilder builder = new AlarmBuilder(alarm).previousStatus(alarm.getCurrentStatus());
        builder = builder.currentStatus(CLEARED);
        builder.clearDate(LocalDateTime.now());
        Alarm clearAlarm = builder.build();

        List<Alarm> alarmList = alarms.get(alarm.getAlarmType().toString());
        alarmList.add(alarmList.indexOf(alarm), clearAlarm);
        alarmList.remove(alarm);

        //check if all unavailable status alarms removed
        for(Alarm a : alarmList) {
            if (a.getAlarmType() == UNIT_UNAVAILABLE && a.getCurrentStatus() != CLEARED) {
                currentState = OFFLINE;
                return true;
            }
        }

        //element is back online
        currentState = ONLINE;
        return true;
    }

    public final void removeClearedAlarms() {

        for(List<Alarm> alist : alarms.values()) {
            Iterator<Alarm> iter = alist.iterator();
            while(iter.hasNext()) {
                if (iter.next().getCurrentStatus() == CLEARED) {
                    iter.remove();
                }
            }
        }
    }

    public final void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public final void setParentId(Integer parentId) {
        this.parentId = parentId;
    }


    public final void setDescription(String description) {
        this.description = description;
    }


    @SuppressWarnings("unchecked")
    public static abstract class NetBuilder<T extends NetBuilder<T,V>, V extends NetworkElement> {
        private final int id;
        private final NeType type;
        private final String name;
        private String ipAddress = "";
        private int parentId = -1;
        private NetworkElementState state = ONLINE;
        private String description = "Not Provided";
        private Map<String, List<Alarm>> alarms;
        private List<Observer> observers;

        public NetBuilder(int id, NeType type, String name) {
            this.id = id;
            this.type = type;
            this.name = name;
            this.alarms = new LinkedHashMap<>();
            this.observers = new ArrayList<>();
        }

        /* Shallow Utility copy cons*/
        public NetBuilder(NetworkElement networkElement) {
            this.id = networkElement.id;
            this.type = networkElement.type;
            this.name = networkElement.name;
            this.ipAddress = networkElement.ipAddress;
            this.parentId = networkElement.parentId;
            this.description = networkElement.description;
            this.alarms  = networkElement.alarms;
            this.observers = new ArrayList<>(); //trans field
            this.state = networkElement.currentState;
        }

        public NetBuilder(NetworkElement networkElement, String name) {
            this.id = networkElement.id;
            this.type = networkElement.type;
            this.name = name;
            this.ipAddress = networkElement.ipAddress;
            this.parentId = networkElement.parentId;
            this.description = networkElement.description;
            this.alarms  = networkElement.alarms;
            this.observers = networkElement.netElementObservers;
        }

        public final T ipAddress(String ipAddress) {
            this.ipAddress = ipAddress;
            return (T)this;
        }

        public final T parentId(int parentId) {
            this.parentId = parentId;
            return (T)this;
        }

        public final T description(String description) {
            this.description = description;
            return (T)this;
        }

        public abstract V build();
    }

    @Override
    public int hashCode() {
        int val = 23;
        val = ((val<<5) - val) + id;
        val = ((val<<5) - val) + type.hashCode();
        val = ((val<<5) - val) + ((name == null)? 0 : name.hashCode());
        val = ((val<<5) - val) + ((ipAddress == null)? 0 : ipAddress.hashCode());
        val = ((val<<5) - val) + ((parentId == 0)? 0 : parentId);
        val = ((val<<5) - val) + ((description == null)? 0: description.hashCode());
        val = ((val<<5) - val) + ((alarms == null)? 0: alarms.hashCode());
        return val;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if((o instanceof NetworkElement) != true)
            return false;

        NetworkElement e = (NetworkElement) o;

        if(id != e.id)
            return false;
        if(type != e.type)
            return false;
        if((name == null) ? (e.name != null): !name.equals(e.name))
            return false;
        if((ipAddress == null) ? (e.ipAddress != null): !ipAddress.equals(e.ipAddress))
            return false;
        if(parentId !=  e.parentId)
            return false;
        if((description== null) ? (e.description != null): !description.equals(e.description))
            return false;
        if(!alarms.equals(e.alarms))
            return false;
        return true;
    }


    public NetworkElementState getState() {
        return currentState;
    }

    public String toString() {
        return "NET ID: "+id+", Type: "+type+", NET ID: "+id+", Name: "+name+", IP_ADDRESS: "+ipAddress+", PARENT_ID: "+parentId+", DESC: "+description +
                ", Active Alarm Count: "+getAlarmCount()+", CURRENT STATE: "+currentState;
    }

    /*Observable interface behaviours */
    public void addObserver(Observer obs) {
        if(obs != null)
            netElementObservers.add(obs);
    }
    public void removeObserver(Observer obs) {
        if(netElementObservers.contains(obs))
            netElementObservers.remove(obs);

    }
    public void updateObservers() {
        for(Observer obs : netElementObservers)
            obs.update(currentState);
    }



    @Override
    public void update(NetworkElementState state) {
        currentState = state;
    }

    @Override
    public int compareTo(NetworkElement o) {
        return name.compareToIgnoreCase(o.name);
    }

    //Not really needed as O(1) look up with hash-maps
    public static final Comparator<NetworkElement> COMPARE_NET_NAME = new Comparator<NetworkElement>() {

        @Override
        public int compare(NetworkElement o1, NetworkElement o2) {
            return o1.compareTo(o2);
        }
    };

    //Not really needed as O(1) look up with hash-maps
    public static final Comparator<NetworkElement> COMPARE_NET_ID= new Comparator<NetworkElement>() {

        @Override
        public int compare(NetworkElement o1, NetworkElement o2) {
            return o1.id-o2.id;
        }
    };

}
