package err.errigal.network_elements;

public enum  AlarmType {

    UNIT_UNAVAILABLE("UNIT UNAVAILABLE", "TECH CALL OUT", 3),
    OPTICAL_FIBRE_LOSS("OPTICAL FIBRE LOSS", "FIRBE LOSS SPECIFIC REMEDY", 2),
    DARK_FIBRE("DARK FIRBE", "DARK LOSS SPECIFIC REMEDY", 1),
    POWER_OUTAGE("POWER OUTAGE", "POWER OUTAGE SPECIFIC REMEDY", 2);


    public enum Status {

        CLEARED("CLEARED",0), MINOR("MINOR", 1), MEDIUM("MEDIUM",2), CRITICAL("CRITICAL",3);

        private int priorityLevel;
        private String priority;

        Status(String priority, int priorityLevel) {
            this.priority = priority;
            this.priorityLevel = priorityLevel;
        }

        public String toString() {
            return priority;
        }

        public int getPriorityLevel() {
            return priorityLevel;
        }
    }

    private String name;
    private String remedy;
    private int index;

    AlarmType(String name, String remedy, int index) {
        this.name = name;
        this.remedy = remedy;
        this.index = index;
    }

    public String toString() {
        return name;
    }

    public String getRemedy() {
        return remedy;
    }

}
