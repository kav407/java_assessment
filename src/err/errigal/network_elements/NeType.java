package err.errigal.network_elements;

public enum NeType {
    HUB("HUB"), NODE("NODE");

    private String name;

    NeType(String name) {
        this.name = name;
    }
    public String toString() {
        return name;
    }
}
