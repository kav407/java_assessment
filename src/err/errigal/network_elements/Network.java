package err.errigal.network_elements;
import java.util.*;
import static err.errigal.network_elements.Result.*;

/*
 *  Purpose: Network Abstraction -  The applications network, holds carriers, hubs and nodes, UI menus interface and provide
 *                                  their functionality through an instance of this class.
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public final class Network {

    private Map<String, Carrier> carriers;

    /* Serialization will invalidate below maps as they will contain duplicate objects, each
     *  map must refer to same object, therefore below maps will be reconstructed using carriers map above
     *  after deserialization
     */
    private transient Map<Integer, Carrier> carrierIds;
    private transient Map<Integer, NetworkElement> netElementIDs;

    private int carrierCount;
    private int hubCount;
    private int nodeCount;


    public Network() {
       carriers = new LinkedHashMap<>();
       netElementIDs = new LinkedHashMap<>();
       carrierIds = new LinkedHashMap<>();
       carrierCount = 0;
       hubCount = 0;
       nodeCount = 0;
    }

    /* cons used to reconstruct network after deserialization*/
    public Network(Network jsonNetwork) {

        this.carriers = new LinkedHashMap<>();
        this.carrierIds = new LinkedHashMap<>();
        this.netElementIDs = new LinkedHashMap<>();

        Map<String, Carrier> jsonNetworkCarriers = jsonNetwork.carriers;

        /* rebuild network from imported from json to restore valid object references */
        for(Map.Entry<String, Carrier> entry : jsonNetworkCarriers.entrySet()) {
            Carrier carrier = new Carrier(entry.getValue());
            carriers.put(carrier.getName(), carrier);
            carrierIds.put(carrier.getId(), carrier);

            for(Hub hub : carrier.getHubList()) {
                netElementIDs.put(hub.getID(), hub);
                for(Node node : hub.getNodes()) {
                    netElementIDs.put(node.getID(), node);
                    hub.addObserver(node);
                }
            }
        }

        carrierCount = jsonNetwork.carrierCount;
        hubCount = jsonNetwork.hubCount;
        nodeCount = jsonNetwork.nodeCount;
    }

    public Result addCarrier(Carrier carrier) {
        if(carrier == null) {
            return NULL_VALUE_GIVEN;
        }
        if(carriers.containsKey(carrier.getName())) {
            return NON_UNIQUE_CARRIER_NAME;
        }
        if(carrier.getChildCount() >0) {
            return CARRIER_CONTAINS_CHILDREN;
        }
        if(carrierIds.containsKey(carrier.getId())) {
            return NON_UNIQUE_ID;
        }

        carrierIds.put(carrier.getId(), carrier);
        carriers.put(carrier.getName(), carrier);
        carrierCount++;
        return SUCCESS;
    }

    public Result addHubToCarrier(String carrierName, Hub hub) {
        if(carrierName == null) {
            return NULL_VALUE_GIVEN;
        }
        if(!carriers.containsKey(carrierName)) {
            return CARRIER_NOT_FOUND;
        }
        if(netElementIDs.containsKey(hub.getID())) {
            return NON_UNIQUE_ID;
        }
        if(carriers.get(carrierName).hubExists(hub.getName())) {
            return NON_UNIQUE_HUB_NAME;
        }
        if(hub.getChildCount() > 0) {
            return HUB_CONTAINS_CHILDREN;
        }

        netElementIDs.put(hub.getID(), hub);
        carriers.get(carrierName).addHub(hub);
        hubCount++;
        return SUCCESS;
    }

    public Result addNodeToHub(String carrierName, String hubName, Node node) {
        if(hubName == null || hubName == null || node == null) {
            return NULL_VALUE_GIVEN;
        }
        if(!carriers.containsKey(carrierName)) {
            return CARRIER_NOT_FOUND;
        }
        if(carriers.get(carrierName).getHubByName(hubName) == null) {
            return HUB_NOT_FOUND;
        }
        if(carriers.get(carrierName).getHubByName(hubName).nodeExists(node.getName())) {
            return NON_UNIQUE_NODE_NAME;
        }
        if(netElementIDs.containsKey(node.getID())) {
            return NON_UNIQUE_ID;
        }

        netElementIDs.put(node.getID(), node);
        carriers.get(carrierName).getHubByName(hubName).addNode(node);
        nodeCount++;
        return SUCCESS;
    }

    public boolean addAlarmToNetworkElement(int netID, Alarm alarm) {
        if(!netElementIDs.containsKey(netID)) {
            return false;
        }

        netElementIDs.get(netID).addAlarm(alarm);
        return true;
    }


    public Carrier getCarrierByName(String name) {
        if(name == null) return null;
        return carriers.get(name);
    }

    public List<Carrier> getNetworkCarriers() {
        List<Carrier> carrierList = new ArrayList<>();
        carriers.forEach((k,v) -> carrierList.add(v));
        return carrierList;
    }
    public List<Hub> getNetworkHubs() {
        List<Hub> hubList = new ArrayList<>();
        for(Carrier car : carriers.values())
            hubList.addAll(car.getHubList());
        return hubList;
    }
    public List<Node> getNetworkNodes() {
        List<Node> nodeList = new ArrayList<>();
        carriers.forEach((k, v) -> {
            List<Hub> list = v.getHubList();
            list.forEach((hub) -> nodeList.addAll(hub.getNodes()));
        });
        return nodeList;
    }

    public Map<NetworkElement, List<Alarm>> getNetworkAlarms() {
        Map<NetworkElement, List<Alarm>> temp = new LinkedHashMap<>();

        for(Carrier car : carriers.values()) {
            List<Hub> hubs = car.getHubList();
            for(Hub hub : hubs) {
                temp.put(hub, hub.getAlarmList());
                List<Node> nodes = hub.getNodes();
                for(Node node : nodes)
                    temp.put(node, node.getAlarmList());
            }
        }
        return temp;
    }

    public boolean carrierNameExists(String name) {
        if(name == null) {
            return false;
        }

        return carriers.containsKey(name);
    }

    public boolean carrierIDExists(int id) {
        return carrierIds.containsKey(id);
    }

    public boolean removeCarrier(String name) {
        if(!carrierNameExists(name))
            return false;

        Carrier carrier = carriers.get(name);

        carrier.getHubList().forEach(hub -> {
            for (Node node : hub.getNodes()) {
                netElementIDs.remove(node.getID(), node);
                hub.removeNode(node);
            }
            netElementIDs.remove(hub.getID(), hub);
            carrier.removeHub(hub);
        });

        carrierCount--;
        return carriers.remove(name, carrier);
    }

    public boolean removeHub(String carrierName, String hubName) {
        if(!carrierNameExists(carrierName)) {
            return false;
        }
        if(!carriers.get(carrierName).hubExists(hubName)) {
            return false;
        }

        hubCount--;
        Hub hub = carriers.get(carrierName).getHubByName(hubName);
        netElementIDs.remove(hub.getID(), hub);
        return carriers.get(carrierName).removeHubByName(hubName);

    }

    public boolean removeNode(String carrierName, String hubName, String nodeName) {
        if(!carrierNameExists(carrierName))
            return false;
        if(!carriers.get(carrierName).hubExists(hubName))
            return false;
        if(!carriers.get(carrierName).getHubByName(hubName).nodeExists(nodeName))
            return false;

        nodeCount--;
        Node node = carriers.get(carrierName).getHubByName(hubName).getNodeByName(nodeName);
        netElementIDs.remove(node.getID(), node);
        return carriers.get(carrierName).getHubByName(hubName).removeNode(node);
    }

    public void clearAlarm(NetworkElement networkElement, Alarm alarm) {
        networkElement.clearAlarm(alarm);
    }



    public String elementNameFromID(int id) {
         return(netElementIDs.get(id) != null) ? netElementIDs.get(id).getName()
                 :(carrierIds.get(id).getName());
    }

    public NetworkElement netElementNameFromID(int id) {
        return netElementIDs.get(id);
    }

    public String carrierNameFromID(int id) {
        if(!carrierIDExists(id)) {
            return "";
        }

        return carrierIds.get(id).getName();
    }

    public Carrier getCarrierByID(int id) {
        if(!carrierIDExists(id)) {
            return null;
        }
        return carrierIds.get(id);
    }

    public boolean netIDExists(int id) {
        return netElementIDs.containsKey(id);
    }

    public int numOfCarriers() {
        return carrierCount;
    }

    public int numOfHubs() {
        return hubCount;
    }

    public int numOfNodes() {
        return nodeCount;
    }

    public int getHubAlarmCount() {
        int alarmCount = 0;
        for(Carrier c : carriers.values())
            for (Hub h : c.getHubList())
                alarmCount+=h.getAlarmCount();
        return alarmCount;
    }

    public List<Hub> getHubsByName() {
        List<Hub> hubList = new ArrayList<>();

        for(Carrier carrier : carrierIds.values()) {
            hubList.addAll(carrier.getHubList());
        }

        return hubList;
     }

    public List<Node> getNodesByName() {
        List<Node> nodeList = new ArrayList<>();

        for(Carrier carrier : carrierIds.values()) {
            List<Hub> hubList = carrier.getHubList();
            for(Hub hub : hubList) {
                nodeList.addAll(hub.getNodes());
            }
        }

        return nodeList;
    }

    public int numNodeAlarmCount() {
        int alarmCount = 0;
        for(Carrier c : carriers.values())
            for (Hub h : c.getHubList())
                for(Node n : h.getNodes())
                    alarmCount+=n.getAlarmCount();
        return alarmCount;
    }

    public int getNetworkAlarmCount() {
        return numNodeAlarmCount()+getHubAlarmCount();
    }


    public void changeCarrierName(Carrier currentCarrier, String name) {
        Carrier newCarrier = new Carrier(name, currentCarrier);

        carriers.remove(currentCarrier.getName(), currentCarrier);
        carrierIds.remove(currentCarrier.getId(), currentCarrier);

        carriers.put(newCarrier.getName(), newCarrier);
        carrierIds.put(newCarrier.getId(), newCarrier);
    }
}
