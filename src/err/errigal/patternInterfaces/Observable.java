package err.errigal.patternInterfaces;

public interface Observable {
    void addObserver(Observer obs);
    void removeObserver(Observer obs);
    void updateObservers();
}
