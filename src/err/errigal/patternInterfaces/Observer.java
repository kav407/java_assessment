package err.errigal.patternInterfaces;
import err.errigal.network_elements.AlarmType;
import err.errigal.network_elements.NetworkElement.NetworkElementState;


public interface Observer {
    void update(NetworkElementState state);
}
