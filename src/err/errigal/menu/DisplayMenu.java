package err.errigal.menu;
import static err.errigal.menu.MenuType.*;
import err.errigal.application.NetworkApplication;
import err.errigal.io.IOValidate;
import err.errigal.network_elements.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/*
 *  Purpose: Display Menu - Text based UI for display network carriers & elements
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class DisplayMenu extends Menu {

    private IMenuDelegator menuDelegator;
    private NetworkApplication netApp;
    private final String menuDisplay    =   "1. Display Entire Network      \t\t2. Display Alarm Status\n"+
                                            "3. Display Network Carriers    \t\t4. Display Network Hubs\n"+
                                            "5. Display Network Nodes       \t\t6. Display Hubs By Carrier\n"+
                                            "7. Display Nodes By Hub        \t\t8. Exit to Main Menu\n";

    protected DisplayMenu(IMenuDelegator menuDelegator, NetworkApplication netApp) {
        super(DISPLAY_MENU, 8);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
    }

    public void displayNetwork() {
        System.out.println("\n**********************************************************************\n");

        Network network = netApp.getNetwork();
        List<Carrier> carrierList = network.getNetworkCarriers();

        if(carrierList.size() == 0) {
            System.out.println("\nNo network entities of this type to display....\n");
            return;
        }

        for(Carrier car : carrierList) {
            System.out.println(car+"\n");
            for(Hub hub: car.getHubList()) {
                System.out.println("\t->"+hub);
                hub.displayChildren();
                System.out.println();
            }
        }
    }

    public void displayNetworkAlarms() {
        System.out.println("\n**********************************************************************\n");

        Network network = netApp.getNetwork();
        if(network.getNetworkAlarmCount() == 0){
            System.out.println("\nNo alarms active or cleared alarms on network....\n");
            return;
        }

        Set<NetworkElement> netElements = network.getNetworkAlarms().keySet();

        netElements.forEach((NetworkElement K) -> {
        System.out.println( "NET ID: "+K.getID()+" NET TYPE: "+K.getType()+", NET NAME: "+K.getName()+", NUM ALARMS: "+K.getAlarmList().size()+
                                ", STATUS: "+K.getState()+", PARENT ID: "+K.getParentID()+" PARENT NAME: "+network.elementNameFromID(K.getParentID())+"\n");
            K.getAlarmList().forEach((Alarm alarm) -> System.out.println("\t->"+alarm+", REMEDY: "+alarm.getRemedy()+"\n"));
        });

    }

    public void displayNetworkCarriers() {
        Network network = netApp.getNetwork();
        if(network.numOfCarriers() == 0) {
            System.out.println("\nNo network entities this type to display....\n");
            return;
        }

        List<Carrier> carrierList = network.getNetworkCarriers();
        System.out.println("\n**********************************************************************\n");
        carrierList.forEach((c) -> System.out.println(c));
        System.out.println("\n**********************************************************************\n");
    }

    public void displayNetworkHubs() {
        Network network = netApp.getNetwork();
        if(network.numOfHubs() == 0) {
            System.out.println("\nNo network entities of this type to display....\n");
            return;
        }

        List<Hub> hubList  = network.getNetworkHubs();
        System.out.println("\n**********************************************************************\n");
        hubList.forEach((h)->System.out.println(h));
        System.out.println("\n**********************************************************************\n");
    }

    public void displayNetworkNodes() {
        Network network = netApp.getNetwork();
        if(network.numOfNodes() ==0) {
            System.out.println("\nNo network entities of this type to display....\n");
            return;
        }

        List<Node> nodeList  = network.getNetworkNodes();
        System.out.println("\n**********************************************************************\n");
        nodeList.forEach((n)->System.out.println(n));
        System.out.println("\n**********************************************************************\n");
    }

    public void displayHubByCarrier() {
        List<Carrier> networkCarriers = netApp.getNetwork().getNetworkCarriers();
        if(networkCarriers.size() == 0) {
            System.out.println("\nNo carriers on network, therefore no hubs....");
            return;
        }

        System.out.println();
        for(int i=0; i< networkCarriers.size(); i++) {
            System.out.println((i+1)+"."+networkCarriers.get(i).getName());
        }

        System.out.print("\nEnter index list number of carrier for which hubs are to be displayed: ");
        int index =  IOValidate.indexInRange(1, networkCarriers.size(), "Enter index list number of carrier for which hubs are to be displayed:, or press -1 to exit: ", "Error, invalid list number");
        if(index == -1) {
            return;
        }

        Carrier selectedCarrier = networkCarriers.get(index-1);

        System.out.println("\n********************* "+selectedCarrier.getName()+"'s Active Hubs *******************************\n");
        for(Hub hub : selectedCarrier.getHubList()) {
            System.out.println(hub);
        }

        System.out.println();

    }

    private void displayNodesByHub() {
        List<Carrier> networkCarriers = netApp.getNetwork().getNetworkCarriers();
        if(networkCarriers.size() == 0) {
            System.out.println("\nNo carriers on network, therefore no nodes....");
            return;
        }

        System.out.println();
        int hubDisplayIndex = 1;
        List<Hub> hubsList = new ArrayList<>();

        for(int i=0; i< networkCarriers.size(); i++) {
            List<Hub> hubs = networkCarriers.get(i).getHubList();
            hubsList.addAll(hubs);
            for(int j=0; j<hubs.size();j++) {
                Hub hub = hubs.get(j);
                System.out.println(hubDisplayIndex+"." +hub.getName()+" ("+netApp.getNetwork().carrierNameFromID(hub.getParentID())+")");
                hubDisplayIndex++;
            }
        }

        System.out.println();

        if(hubsList.size() == 0) {
            System.out.println("Sorry hub on network....");
            return;
        }

        int index =  IOValidate.indexInRange(1, hubsList.size(), "\nEnter list number of hub to display children nodes, or press -1 to exit: ", "Error, invalid list number");
        if(index == -1) {
            return;
        }

        Hub selectedHub = hubsList.get(index-1);

        if(selectedHub.getNodes().size() == 0) {
            System.out.println("Sorry, no nodes on this hub");
            return;
        }

        System.out.println("\n********************* "+selectedHub.getName()+"("+netApp.getNetwork().carrierNameFromID(selectedHub.getParentID())+" Active Nodes *******************************\n");

        for(Node node : hubsList.get(index-1).getNodes()) {
            System.out.println(node);
        }

        System.out.println();
    }

    @Override
    protected String getMenuDisplay() {
        return menuDisplay;
    }

    @Override
    protected void processInput(int menuOption) {
        switch (menuOption) {
            case 1: displayNetwork();
                break;
            case 2: displayNetworkAlarms();
                break;
            case 3: displayNetworkCarriers();
                break;
            case 4: displayNetworkHubs();
                break;
            case 5: displayNetworkNodes();
                break;
            case 6: displayHubByCarrier();
                break;
            case 7: displayNodesByHub();
                break;
            case 8: menuDelegator.menuTransition(MAIN_MENU);
        }
    }

    @Override
    public String toString() {
        return this.getMenuType().getName();
    }

}
