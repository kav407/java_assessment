package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.io.IOValidate;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.*;
import err.errigal.network_elements.Alarm.AlarmBuilder;
import java.time.LocalDateTime;
import java.util.*;
import static err.errigal.menu.MenuType.MAIN_MENU;
import static err.errigal.network_elements.AlarmType.*;
import static err.errigal.network_elements.AlarmType.Status.CLEARED;


/*
 *  Purpose: Text UI for Alarms in network app - Manages the adding/removal of alarms in app
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class AlarmMenu extends Menu {

    private IMenuDelegator menuDelegator;
    private NetworkApplication netApp;
    private final String menuDisplay =  "1. Create Alarm For Hub        \t\t2. Create Alarm For Node\n"+
                                        "3. Clear Existing Alarm        \t\t4. Removed all cleared network alarms\n"+
                                        "5. Exit to main menu\n";


    protected AlarmMenu(IMenuDelegator menuDelegator, NetworkApplication netApp) {
        super(MenuType.ALARM_MENU, 5);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
    }

    public void createHubAlarm() {

        Network network = netApp.getNetwork();

        String carrierName = InputHandler.readInputWithPrompt("Enter name of Carrier where hub resides: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter name of Carrier where hub resides: ", "carrier not found", false);

        if(carrierName.equals("-1")) {
            return;
        }

        List<Hub> hubList = network.getCarrierByName(carrierName).getHubList();

        if(hubList.size()== 0) {
            System.out.println("Sorry no hubs on this carrier");
            return;
        }

        System.out.println("\n**********************************************************************\n");
        System.out.println("                  Active Hubs Under Carrier: "+carrierName+"              ");
        System.out.println("\n**********************************************************************\n");

        for(int i=1;i<=hubList.size();i++) {
            System.out.println(i + ". " + hubList.get(i - 1));
        }

        System.out.println();
        int index =  IOValidate.indexInRange(1, hubList.size(), "Enter hub list number, or press -1 to exit: ", "Error, invalid list number");

        if(index == -1) {
            return;
        }

        buildAlarmSubMenu(network.netElementNameFromID(hubList.get(index-1).getID()));
    }

    public void createNodeAlarm() {

        Network network = netApp.getNetwork();

        String carrierName = InputHandler.readInputWithPrompt("Enter name of Carrier where node resides: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter name of Carrier where node resides: ", "carrier not found", false);

        if(carrierName.equals("-1")) {
            return;
        }

        List<Hub> hubList = network.getCarrierByName(carrierName).getHubList();

        if(hubList.size()== 0) {
            System.out.println("Sorry no hubs on this carrier, therefore no nodes");
            return;
        }

        System.out.println("\n**********************************************************************\n");
        System.out.println("                  Active Nodes Under Carrier: "+carrierName+"       ");
        System.out.println("\n**********************************************************************\n");

        List<Node> nodeList = new ArrayList<>();
        int displayIndex = 0;

        for(int i=0; i<hubList.size(); i++) {

            System.out.println(hubList.get(i));
            List<Node> tempNodes = hubList.get(i).getNodes();
            nodeList.addAll(tempNodes);

            for(int j=0; j<tempNodes.size(); j++) {
                System.out.println("\t\t->" + " " + (++displayIndex) + ". " + tempNodes.get(j));
            }
        }

        System.out.println();

        if(nodeList.size() == 0) {
            System.out.println("Sorry no nodes under this carrier....");
            return;
        }

        int index =  IOValidate.indexInRange(1, nodeList.size(), "\nEnter list number of node to get new alarm, or press -1 to exit: ", "Error, invalid list number");

        if(index == -1) {
            return;
        }

        buildAlarmSubMenu(nodeList.get(index-1));
    }


    public void clearAlarm() {

        Network network = netApp.getNetwork();

        if(network.getNetworkAlarmCount() == 0) {
            System.out.println("No alarms on network to clear");
            return;
        }

        String carrierName = InputHandler.readInputWithPrompt("Enter name of Carrier where alarm resides: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter name of Carrier where alarm resides: ", "carrier not found", false);

        if(carrierName.equals("-1")) {
            return;
        }

        List<Hub> hubList = network.getCarrierByName(carrierName).getHubList();

        if(hubList.size()== 0) {
            System.out.println("Sorry no net elements on this carrier");
            return;
        }

        System.out.println("\n**********************************************************************\n");
        System.out.println("            Active Alarms On Elements Under Carrier: "+carrierName+"      ");
        System.out.println("\n**********************************************************************\n");

        int displayIndex = 0;

        List<NetworkElement> alarmElements = new ArrayList<>();

        for(Hub hub : hubList) {

            if(hub.getAlarms().size() >0) {

                System.out.println((++displayIndex)+". "+hub);

                for(Alarm a : hub.getAlarmList()) {
                    System.out.println("\t\t-> " + a);
                }

                alarmElements.add(hub);
            }

            for(Node n : hub.getNodes()) {

                if (n.getAlarms().size() > 0) {

                    System.out.println((++displayIndex) + ". " + n);

                    for (Alarm b : n.getAlarmList()) {
                        System.out.println("\t\t-> " + b);
                    }

                    System.out.println();
                    alarmElements.add(n);
                }
            }
        }

        System.out.println();

        int index =  IOValidate.indexInRange(1, alarmElements.size(), "Enter list index number of network element which has alarm to be cleared, or press -1 to exit: ", "Error, invalid list number");

        if(index == -1) {
            return;
        }

        clearAlarm(alarmElements, index-1);
    }

    private void clearAlarm(List<NetworkElement> elements, int elementIndex) {

        System.out.println();
        Network network = netApp.getNetwork();
        List<Alarm> alarms = elements.get(elementIndex).getAlarmList();

        for(int i = 0; i<alarms.size(); i++) {
            System.out.println((i + 1) + ". " + alarms.get(i));
        }

        System.out.println();
        int index =  IOValidate.indexInRange(1, alarms.size(), "Enter list index  number for alarm to be cleared, or press -1 to exit: ", "Error, invalid list number");
        if(index == -1) {
            return;
        }

        if(alarms.get(index-1).getCurrentStatus() == CLEARED) {
            System.out.println("This alarm is already cleared......please go to remove cleared alarms option to remove this");
            return;
        }


        network.clearAlarm(elements.get(elementIndex), alarms.get(index-1));
        System.out.println("\n.....successfully cleared alarm "+alarms.get(index-1).getAlarmType()+", on element "+elements.get(elementIndex).getName());
        System.out.println("Cleared alarm will reside on network element until also removed");
    }


    private void buildAlarmSubMenu(NetworkElement element) {

        System.out.println("\n**********************************************************************\n");
        System.out.println("                Alarm Builder For: "+element.getType()+" "+element.getName());
        System.out.println("\n**********************************************************************\n");

        AlarmType[] typesList = AlarmType.values();

        for(int i = 1; i<=typesList.length; i++) {
            System.out.println(i + ". " + typesList[i - 1]);
        }

        System.out.println();

        int index =  IOValidate.indexInRange(1, typesList.length, "Enter list number for type of alarm, or press -1 to exit: ", "Error, invalid list number");

        if(index == -1) {
            return;
        }

        AlarmType alarmType = typesList[index-1];

        System.out.println("\nYou selected alarm: "+alarmType+" alarm type\n" );

        List<Status> statusList = new ArrayList<>(Arrays.asList(Status.values()));
        statusList.remove(0); //Remove cleared status from options, separate menu option for this

        System.out.println("Please select appropriate status for your alarm\n");

        for(int i = 0; i<statusList.size(); i++) {
            System.out.println((i + 1) + ". " + statusList.get(i));
        }

        index =  IOValidate.indexInRange(1, statusList.size(), "\nEnter alarm status list number, or press -1 to exit: ", "Error, invalid list number");
        if(index == -1) {
            return;
        }

        Status statusType = statusList.get(index-1);

        int alarmID = IOValidate.validAlarmId("Enter ID for Alarm, or press -1 to exit: ", "Not a unique alarm id");
        if(alarmID == -1) {
            return;
        }

        buildAlarm(element, alarmType, statusType, alarmID);
    }

    public void removeClearedAlarms() {
        Map<NetworkElement, List<Alarm>> netAlarms = netApp.getNetwork().getNetworkAlarms();

        for(Map.Entry<NetworkElement, List<Alarm>> entry : netAlarms.entrySet()) {
            entry.getKey().removeClearedAlarms();
        }

        System.out.println("\n.....successfully removed all cleared alarm on network");
    }

    private void buildAlarm(NetworkElement element, AlarmType alarmType, Status status, int alarmID) {
        Network network = netApp.getNetwork();
        Alarm alarm  = new AlarmBuilder(alarmID, LocalDateTime.now(), alarmType).currentStatus(status).build();
        System.out.println("\n.....Successfully added "+alarmType+" alarm to net element -> "+element.getName()+"\n");
        network.addAlarmToNetworkElement(element.getID(), alarm);
    }

    @Override
    protected String getMenuDisplay() {
        return menuDisplay;
    }

    protected void processInput(int input) {
        switch(input) {
            case 1: createHubAlarm();
            break;
            case 2: createNodeAlarm();
            break;
            case 3: clearAlarm();
            break;
            case 4: removeClearedAlarms();
            break;
            case 5: menuDelegator.menuTransition(MAIN_MENU);
        }
    }

    @Override
    public String toString() {
        return this.getMenuType().getName();
    }

}
