package err.errigal.menu;
import static err.errigal.menu.MenuType.*;
import err.errigal.application.NetworkApplication;
import err.errigal.io.IOValidate;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.*;
import err.errigal.sorting_searching.ModifiedBinarySearch;

import java.util.Collections;
import java.util.List;

/*
 *  Purpose: Display Menu - Text based UI for display network carriers & elements
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class StatusMenu extends Menu {

    private IMenuDelegator menuDelegator;
    private ModifiedBinarySearch bs;
    private NetworkApplication netApp;
    private final String menuDisplay    =   "1. Display Carrier Status By Name  \t\t2. Display Carrier Status By ID\n"+
                                            "3. Display Hub Status By Name      \t\t4. Display Node Status By Name\n"+
                                            "5. Display Hub/Node Status By ID   \t\t7. Exit to Main Menu\n";

    protected StatusMenu(IMenuDelegator menuDelegator, NetworkApplication netApp) {
        super(STATUS_MENU, 7);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
        bs = new ModifiedBinarySearch();
    }


    @Override
    protected String getMenuDisplay() {
        return menuDisplay;
    }


    public void statusByCarrierName() {
        Network net = netApp.getNetwork();
        List<Carrier> carriers = net.getNetworkCarriers();

        if(carriers.size() == 0) {
            System.out.println("No carriers added to network.....");
            return;
        }

        String carrierName = InputHandler.readInputWithPrompt("Enter name of carrier for status display: ");
        carrierName = IOValidate.validCarrierName(net, carrierName, "Enter name of carrier for status display: ", "no such carrier exists", false);
        if(carrierName.equals("-1")) {
            return;
        }

        Carrier carrier = net.getCarrierByName(carrierName);

        System.out.println("\n**********************************************************************\n");
        System.out.println("                  Status for Carrier: "+carrier.getName()+"\n");
        System.out.println("**********************************************************************\n");

        displayStatusByCarrier(carrier);
    }


    public void statusByCarrierID() {
        Network net = netApp.getNetwork();
        List<Carrier> carriers = net.getNetworkCarriers();

        if(carriers.size() == 0) {
            System.out.println("No carriers added to network.....");
            return;
        }

        int carrierID = InputHandler.readInt("Enter ID of carrier for status display: ", "not a valid carrier ID");
        String id = IOValidate.validCarrierID(net, Integer.toString(carrierID), "Enter ID of carrier for status display: ", "no such carrier ID exists", false);
        if(id.equalsIgnoreCase("exit")) {
            return;
        }

        Carrier carrier = net.getCarrierByID(Integer.parseInt(id));

        System.out.println("\n**********************************************************************\n");
        System.out.println("                  Status for Carrier: "+carrier.getName()+"\n");
        System.out.println("**********************************************************************\n");

        displayStatusByCarrier(carrier);

    }


    public void statusByHubName() {
        Network net = netApp.getNetwork();
        if(net.numOfCarriers() == 0) {
            System.out.println("\nNo carriers on Network, therefore no hubs....\n");
            return;
        }

        String hubName = InputHandler.readInputWithPrompt("Enter name of hub for status display: ");
        List<Hub> networkHubs = net.getNetworkHubs();
        Collections.sort(networkHubs, NetworkElement.COMPARE_NET_NAME);
        int firstIndex = bs.getFirstOccurrenceString(networkHubs, hubName);
        int lastIndex = bs.getLastOccurrenceString(networkHubs, hubName);

        if(firstIndex == -1 || lastIndex == -1) {
            System.out.println("\nNo hubs on network found by name: "+hubName+"......\n");
            return;
        }

        List<Hub> hubsByName = networkHubs.subList(firstIndex, lastIndex+1);

        System.out.println("\n********************************************************************************************************************************************\n");
        System.out.println("                  Status for Hubs Under Name: "+hubName+"\n");
        System.out.println("********************************************************************************************************************************************\n");

        for(int i=0; i<hubsByName.size();i++) {
            System.out.print("\t->"+hubsByName.get(i).getName()+", Type: Hub, ID: "+hubsByName.get(i).getID()+", Current Status: "+hubsByName.get(i).getState());
            System.out.println(", Carrier: "+net.carrierNameFromID(hubsByName.get(i).getParentID())+"\n");
        }

    }

    public void statusByNodeName() {
        Network net = netApp.getNetwork();
        if(net.numOfCarriers() == 0) {
            System.out.println("\nNo carriers on Network, therefore no hubs....\n");
            return;
        }

        String nodeName = InputHandler.readInputWithPrompt("Enter name of node for status display: ");
        List<Node> networkNodes = net.getNetworkNodes();
        Collections.sort(networkNodes, NetworkElement.COMPARE_NET_NAME);
        int firstIndex = bs.getFirstOccurrenceString(networkNodes, nodeName);
        int lastIndex = bs.getLastOccurrenceString(networkNodes, nodeName);

        if(firstIndex == -1 || lastIndex == -1) {
            System.out.println("\nNo nodes on network found by name: "+nodeName+"......\n");
            return;
        }

        List<Node> nodesByName = networkNodes.subList(firstIndex, lastIndex+1);

        System.out.println("\n********************************************************************************************************************************************\n");
        System.out.println("                  Status for Nodes Under Name: "+nodeName+"\n");
        System.out.println("********************************************************************************************************************************************\n");

        for(int i=0; i<nodesByName.size();i++) {
            Node node = nodesByName.get(i);
            NetworkElement parent = net.netElementNameFromID(node.getParentID());
            System.out.print("\t->" +node.getName() + ", Type: Hub, ID: "+node.getID() + ", Current Status: "+node.getState());
            System.out.println(", Parent Hub: "+parent.getName()+", Parent Type: "+parent.getType()+", Carrier: "+net.getCarrierByID(parent.getParentID()).getName()+"\n");
        }
    }

    public void statusByNetworkElementID() {
        Network net = netApp.getNetwork();
        List<Carrier> carriers = net.getNetworkCarriers();

        if(carriers.size() == 0) {
            System.out.println("No carriers added to network, therefore no hubs/nodes.....");
            return;
        }

        int netID = InputHandler.readInt("Enter ID of hub/node for status display: ", "invalid input");
        netID = IOValidate.validNetIDNotExists(net, netID, "Enter ID of hub/node for status display: ", "no such Hub/Node ID exists");
        if(netID == -1) {
            return;
        }

        NetworkElement networkElement = net.netElementNameFromID(netID);

        System.out.println("\n**********************************************************************\n");
        System.out.println("                  Status for Network Element: "+networkElement.getName()+"\n");
        System.out.println("**********************************************************************\n");

        System.out.println("\t->"+networkElement.getName()+", Type: "+networkElement.getType()+", ID: "+networkElement.getID()+", Current Status: "+networkElement.getState()+"\n");

    }

    private void displayStatusByCarrier(Carrier carrier) {
        List<Hub> hubs = carrier.getHubList();

        for(Hub hub : hubs) {
            List<Node> nodes = hub.getNodes();
            System.out.println("\t->"+hub.getName()+", Type: Hub, ID: "+hub.getID()+", Current Status: "+hub.getState());
            for(Node node : nodes) {
                System.out.println("\t\t->"+node.getName()+", Type: Node, ID: "+node.getID()+", Current Status: "+node.getState());
            }
        }

        System.out.println();

    }

    @Override
    protected void processInput(int menuOption) {
        switch (menuOption) {
            case 1: statusByCarrierName();
                break;
            case 2: statusByCarrierID();
                break;
            case 3: statusByHubName();
                break;
            case 4: statusByNodeName();
                break;
            case 5: statusByNetworkElementID();
                break;
            case 6: menuDelegator.menuTransition(MAIN_MENU);
                break;
            case 7: menuDelegator.menuTransition(MAIN_MENU);
        }
    }


    @Override
    public String toString() {
        return this.getMenuType().getName();
    }

}
