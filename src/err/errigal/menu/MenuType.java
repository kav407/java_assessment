package err.errigal.menu;

public enum MenuType {

    MAIN_MENU("MAIN MENU", 0), DISPLAY_MENU("DISPLAY NETWORK ELEMENTS", 1), REMOVE_MENU("REMOVE NETWORK ELEMENTS", 2),
    EDIT_MENU("EDIT NETWORK ELEMENTS", 3), ALARM_MENU("ALARM CONFIG", 4),
    ADD_MENU("ADD NETWORK ELEMENT", 5), EXIT("EXIT", 6), SEARCH_MENU("NETWORK ELEMENT SEARCH",7),
    STATUS_MENU("DISPLAY ELEMENT STATUS ",8);;

    private String name;
    private int index;

    MenuType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }
}
