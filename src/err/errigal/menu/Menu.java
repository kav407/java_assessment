package err.errigal.menu;

import err.errigal.io.InputHandler;
import err.errigal.io.IOValidate;
import err.errigal.network_elements.Network;
import err.errigal.patternInterfaces.Observable;
import err.errigal.patternInterfaces.Observer;

import java.util.ArrayList;
import java.util.List;

/*
 *  Purpose: Base Abstract Type Menu - Serves as main base class for all applications menu
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public abstract class Menu {

    private MenuType menuType;
    public final int MAX_MENU_OPTION;

    protected Menu(MenuType menuType, int maxMenuOptions) {
        this.menuType = menuType;
        this.MAX_MENU_OPTION = maxMenuOptions;
    }

    protected String getMenuTitle() {

        return  "**********************************************************************\n"+
                "            " +this+"             \n"+
                "**********************************************************************\n";
    }

    protected final MenuType getMenuType() {
        return menuType;
    }

    protected final boolean handleMenuAction() {

        int menuOption = InputHandler.readInt("Enter Menu Option: ", "Invalid Menu Option!");
        boolean validInput = IOValidate.integerInRange(menuOption, MAX_MENU_OPTION, "Error: no menu such option ");

        if(menuOption <0 || !validInput) return false;

        processInput(menuOption);

        return true;
    }

    protected abstract String getMenuDisplay();
    protected abstract void processInput(int menuOption);
    public abstract String toString();

}