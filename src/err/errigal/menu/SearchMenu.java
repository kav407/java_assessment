package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.*;
import err.errigal.sorting_searching.ModifiedBinarySearch;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static err.errigal.menu.MenuType.MAIN_MENU;
import static err.errigal.menu.MenuType.SEARCH_MENU;


/*
 *  Purpose: Search Menu - Text based UI for searching net app, searches for network elements in network
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class SearchMenu extends Menu {

    private IMenuDelegator menuDelegator;
    private NetworkApplication netApp;
    private ModifiedBinarySearch bs;
    private final String menuDisplay    =   "1. Hub name search     \t\t2. Hub ID search\n"+
                                            "3. Node name search    \t\t4. Node ID search\n" +
                                            "5. Exit";

    protected SearchMenu(IMenuDelegator menuDelegator, NetworkApplication netApp) {
        super(SEARCH_MENU, 5);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
        bs = new ModifiedBinarySearch();
    }

    public void hubNameSearch() {

        Network network = netApp.getNetwork();
        String hubName = InputHandler.readInputWithPrompt("Enter hub name for search for: ");

        List<Hub> hubList = new ArrayList<>();

        //could just add names that are equal to target here
        for(Carrier c : network.getNetworkCarriers()) {
            for (Hub b : c.getHubList()) {
                hubList.add(b);
            }
        }

        //sorting with comparators and then binary search
        Collections.sort(hubList);
        int firstOccIndex = bs.getFirstOccurrenceString(hubList, hubName);
        int lastOccIndex = bs.getLastOccurrenceString(hubList, hubName);

        if(firstOccIndex == -1 || hubList.size() == 0){
            System.out.println("Hub name not found on network......");
            return;
        }

        //Sub-List of matches hubs under provided name
        List<Hub> foundNameList = hubList.subList(firstOccIndex, lastOccIndex+1);  //Hub name(s) found

        System.out.println("*** Hub name(s) found ****");
        for(Hub hub : foundNameList) {
            System.out.print("\nCarrier: "+network.elementNameFromID(hub.getParentID())+": ");
            System.out.println("\t->"+hub+"\n");
        }
    }

    public void hubIDSearch() {
        Network network = netApp.getNetwork();
        int hubID = InputHandler.readInt("Enter hub ID to search for: ", "Invalid Input");
        NetworkElement element = network.netElementNameFromID(hubID);

        if(element == null) {
            System.out.println("** Hub ID not found ***");
            return;
        }

        //Kinda of hacky, but it wont fail
        try {
            Hub hub = (Hub)element;
            System.out.println("** Hub ID found ***");
            System.out.println(hub+" - > Parent Element: "+network.elementNameFromID(hub.getParentID()));
        }catch (ClassCastException e) {
            System.out.println("Sorry no hub element under this id: "+hubID);
        }
    }

    public void nodeNameSearch() {

        Network network = netApp.getNetwork(); //This is need in case of imported network
        String hubName = InputHandler.readInputWithPrompt("Enter node name for search for: ");

        List<Node> nodeList = new ArrayList<>();

        //again could just add names that are equal to target here
        for(Carrier c : network.getNetworkCarriers()) {
            for (Hub b : c.getHubList()) {
                for (Node n : b.getNodes()) {
                    nodeList.add(n);
                }
            }
        }

        //sorting with comparators and binary search
        Collections.sort(nodeList);
        int firstOccIndex = bs.getFirstOccurrenceString(nodeList, hubName);
        int lastOccIndex = bs.getLastOccurrenceString(nodeList, hubName);

        if(firstOccIndex == -1){
            System.out.println("Node name not found on network......");
            return;
        }

        List<Node> foundNameList = nodeList.subList(firstOccIndex, lastOccIndex+1);  //Hub name(s) found

        System.out.println("*** Node name(s) found ****");
        for(Node node : foundNameList) {
            NetworkElement par = network.netElementNameFromID(node.getParentID());
            String carrierName = network.elementNameFromID(par.getID());
            System.out.print("\nCarrier: "+carrierName +": ");
            System.out.println("\t->"+node+"\n");
        }
    }

    public void nodeIDSearch() {
        Network network = netApp.getNetwork();
        int nodeID = InputHandler.readInt("Enter node ID to search for: ", "Invalid Input");
        NetworkElement element = network.netElementNameFromID(nodeID);

        if(element == null) {
            System.out.println("** Node ID not found ***");
            return;
        }

        //Again kinda of hacky, but wont fail
        try {
            Node node = (Node)element;
            System.out.println("** Node ID found ***");
            System.out.println(node+" - > Parent Element: "+network.elementNameFromID(node.getParentID()));
        }catch (ClassCastException e) {
            System.out.println("Sorry no node element under this id: "+nodeID);
        }
    }

    @Override
    protected String getMenuDisplay() {
            return menuDisplay;
    }

    protected void processInput(int input) {
        switch(input) {
            case 1: hubNameSearch();
            break;
            case 2: hubIDSearch();
            break;
            case 3: nodeNameSearch();
            break;
            case 4: nodeIDSearch();
            break;
            case 5: menuDelegator.menuTransition(MAIN_MENU);
        }
    }

    @Override
    public String toString() {
        return this.getMenuType().getName();
    }

}

