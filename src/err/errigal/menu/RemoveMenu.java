package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.io.IOValidate;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.Carrier;
import err.errigal.network_elements.Hub;
import err.errigal.network_elements.Network;
import static err.errigal.menu.MenuType.*;

/*
 *  Purpose: Text Based Remove UI Menu - Handles the removal of components for network application
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class RemoveMenu extends Menu {

    private IMenuDelegator menuDelegator;
    private NetworkApplication netApp;
    private final String menuDisplay    =   "1. Remove Carrier From Network \t\t2. Remove Hub From Carrier\n"+
                                            "3. Remove Node From Hub        \t\t4. Exit to Main Menu\n";

    protected RemoveMenu(IMenuDelegator menuDelegator, NetworkApplication netApp) {
        super(REMOVE_MENU, 4);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
    }

    public void removeCarrier() {

        Network network = netApp.getNetwork();

        if(network.numOfCarriers() == 0) {
            System.out.println("No Carriers added to network to remove");
            return;
        }

        String carrierName = InputHandler.readInputWithPrompt("Enter Name for new Carrier(Unique): ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter Name for new Carrier(Unique), or press -1 to exit: ", "must be unique.", false);

        if(carrierName.equals("-1")) {
            return;
        }

        boolean res = network.removeCarrier(carrierName);

        if(res) {
            System.out.println("Successfully removed carrier "+carrierName+" and its child elements");
        }
        else {
            System.out.println("There was a problem removing carrier "+carrierName);
        }
    }

    public void removeHub() {
        Network network = netApp.getNetwork();

        if(network.numOfHubs() == 0) {
            System.out.println("No hubs added to network to remove");
            return;
        }

        String carrierName = InputHandler.readInputWithPrompt("Enter name Of carrier that needs hub removal: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter Name Of Carrier that need Hub removal: ", "Error carrier does not exist", false);

        if(carrierName.equals("-1")) {
            return;   //User request to exit
        }

        Carrier carrier = network.getCarrierByName(carrierName);

        if(carrier.getChildCount() == 0 ) {
            System.out.println("No hub(s) on this carrier, therefore no hubs to remove");
            return;
        }

        String hubName = InputHandler.readInputWithPrompt("Name of Hub to be removed: ");
        hubName = IOValidate.validHubName(carrier, hubName, "Name of Hub to be removed: ", "hub does not exist", false);
        if(hubName.equalsIgnoreCase("-1")) {
            return;      //User request to exit
        }

        boolean res = network.removeHub(carrierName, hubName);

        if(res) {
            System.out.println("\n.....successfully removed Hub " + hubName + " from Carrier " + carrierName + "\n");
        }
        else {
            System.out.println("There was a problem removing Hub " + hubName + " from Carrier " + carrierName + "\n");
        }
    }

    public void removeNode() {

        Network network = netApp.getNetwork();

        if(network.numOfNodes() == 0) {
            System.out.println("No nodes added to network to remove");
            return;
        }

        String carrierName = InputHandler.readInputWithPrompt("Name of Carrier that needs Hub removal: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Name of Carrier that needs Node removal: ", "carrier does not exist", false);

        if(carrierName.equals("-1")) {
            return;   //User request to exit
        }

        Carrier carrier = network.getCarrierByName(carrierName);

        if(carrier.getChildCount() == 0 ) {
            System.out.println("No hub(s) on this carrier, therefore no nodes to remove");
            return;
        }

        String hubName = InputHandler.readInputWithPrompt("Enter the name of the hub parent hub of the node : ");
        hubName = IOValidate.validHubName(carrier, hubName, "Enter the name of the hub parent hub of the node : ", "hub does not exist", false);

        if(hubName.equals("-1")) {
            return;      //User request to exit
        }

        Hub hub = carrier.getHubByName(hubName);

        if(hub.getChildCount() == 0 ) {
            System.out.println("No node(s) on this hub, therefore no node(s) to remove");
            return;
        }

        String nodeName = InputHandler.readInputWithPrompt("Name of Node to be removed: ");
        nodeName = IOValidate.validNodeName(hub, nodeName, "Name of Node to be removed: ","invalid node name", false);

        if(nodeName.equals("-1")) {
            return;       //User request to exit addHub
        }

        boolean res = network.removeNode(carrierName, hubName, nodeName);

        if(res) {
            System.out.println("\n....successfully removed node " + nodeName + " from Hub " + hubName + " on carrier " + carrierName + "\n");
        }
        else {
            System.out.println("\nThere was a problem removing node " + nodeName + " from hub " + hubName + " on carrier " + carrierName + "\n");
        }
    }

    @Override
    protected String getMenuDisplay() {
        return menuDisplay;
    }

    protected void processInput(int input) {
        switch(input) {
            case 1: removeCarrier();
            break;
            case 2: removeHub();
            break;
            case 3: removeNode();
            break;
            case 4: menuDelegator.menuTransition(MAIN_MENU);
        }
    }

    @Override
    public String toString() {
        return this.getMenuType().getName();
    }
}