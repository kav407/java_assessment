package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.io.IOValidate;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.Carrier;
import err.errigal.network_elements.Hub;
import err.errigal.network_elements.Network;
import err.errigal.network_elements.Node;
import java.util.List;
import static err.errigal.menu.MenuType.*;

/*
 *  Purpose: Edit Menu - Text based UI for Edit menu, handles the editing of carriers and net elements
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class EditMenu extends Menu {

    private IMenuDelegator menuDelegator;
    private NetworkApplication netApp;
    private final String menuDisplay    =   "1. Edit Carrier Name   \t\t2. Edit Hub Name\n"+
                                            "3. Edit Node Name      \t\t4. Exit to Main Menu\n";

    protected EditMenu(IMenuDelegator menuDelegator, NetworkApplication netApp) {
        super(EDIT_MENU, 4);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
    }

    public void editCarrierName() {
        Network network = netApp.getNetwork();
        String carrierName = InputHandler.readInputWithPrompt("Enter name for Carrier to be edited: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter name for Carrier to be edited: ", "carrier name not found", false);
        if(carrierName.equals("-1")) {
            return;
        }

        editCarrierName(network.getCarrierByName(carrierName));
    }

    private void editCarrierName(Carrier carrier) {
        System.out.println("\nCurrent carrier name: "+carrier.getName());
        String newName = InputHandler.readInputWithPrompt("\nPlease enter new name for carrier "+carrier.getName()+": ");
        newName = IOValidate.validCarrierName(netApp.getNetwork(), newName, "Enter name for Carrier to be edited: ", "new carrier name must be unqiue", true);
        if(newName.equals("-1")) {
            return;
        }

        netApp.getNetwork().changeCarrierName(carrier, newName);
        System.out.println("\n.....successfully changed carrier name: "+carrier.getName()+" to "+newName);
    }


    public void editHubName() {
        Network network = netApp.getNetwork();
        String carrierName = InputHandler.readInputWithPrompt("Enter name for Carrier which hub is on: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter name for Carrier which hub is on: ", "carrier name not found", false);
        if(carrierName.equals("-1")) {
            return;
        }

        List<Hub> hubs = network.getCarrierByName(carrierName).getHubList();

        if(hubs.size() == 0) {
            System.out.println("Sorry, no hubs on this carrier to edit");
            return;
        }

        System.out.println();

        for(int i=0; i<hubs.size(); i++) {
            System.out.println((i + 1) + ". " + hubs.get(i));
        }

        System.out.println();
        int index =  IOValidate.indexInRange(1, hubs.size(), "Enter hub list number for hub to be edited, or press -1 to exit: ", "Error, invalid list number");
        if(index == -1) {
            return;
        }

        editHubName(network.getCarrierByName(carrierName), hubs.get(index-1));
    }

    private void editHubName(Carrier parent, Hub hub) {
        System.out.println("\nCurrent Hub name: "+hub.getName());
        String newName = InputHandler.readInputWithPrompt("\nPlease enter new name for hub "+hub.getName()+": ");
        newName = IOValidate.validHubName(parent, newName, "Please enter new name for hub: ", "new hub name must be unique", true);
        if(newName.equals("-1") == true) {
            return;
        }

        parent.changeHubName(hub, newName);
        System.out.println("\n.....successfully changed hub name: "+hub.getName()+" to "+newName+" on carrier "+parent.getName());
    }

    public void editNodeName() {
        Network network = netApp.getNetwork();
        String carrierName = InputHandler.readInputWithPrompt("\nEnter name for Carrier which contains node to be edited: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter name for Carrier which contains node to be edited: ", "carrier name not found", false);
        if(carrierName.equals("-1")) {
            return;
        }

        List<Hub> hubs = network.getCarrierByName(carrierName).getHubList();

        if(hubs.size() == 0) {
            System.out.println("Sorry, no hubs on this carrier, therefore no nodes to edit");
            return;
        }
        System.out.println();
        for(int i=0; i<hubs.size(); i++) {
            System.out.println((i + 1) + ". " + hubs.get(i));
            for(Node n : hubs.get(i).getNodes())
                System.out.println("\t\t\t->"+n);
        }

        System.out.println();
        int index =  IOValidate.indexInRange(1, hubs.size(), "Enter hub list number that has node to be edited, or press -1 to exit: ", "Error, invalid list number");
        if(index ==-1) {
            return;
        }

        if(hubs.get(index-1).getChildCount() == 0) {
            System.out.println("Sorry no nodes on this hub to edit");
            return;
        }

        editNodeName(hubs.get(index-1));

    }

    private void editNodeName(Hub parentHub) {
        List<Node> nodes = parentHub.getNodes();
        System.out.println();

        for(int i=0; i<nodes.size();i++) {
            System.out.println((i + 1) + ". " + nodes.get(i));
        }

        System.out.println();

        int index =  IOValidate.indexInRange(1, nodes.size(), "Enter node list for node to be edited, or press -1 to exit: ", "Error, invalid list number");
        if(index == -1) {
            return;
        }

        String nodeName = InputHandler.readInputWithPrompt("Enter new name for node "+nodes.get(index-1).getName()+": ");
        nodeName = IOValidate.validNodeName(parentHub, nodeName, "Enter new name for node: ", "node name must not be an existing node name on hub", true);
        if(nodeName.equals("-1")) {
            return;
        }

        parentHub.changeNodeName(nodes.get(index-1),nodeName);
        System.out.println("\n.....successfully changed node name: "+nodes.get(index-1)+" to "+nodeName);
    }

    @Override
    protected String getMenuDisplay() {
        return menuDisplay;
    }

    protected void processInput(int input) {
        switch(input) {
            case 1: editCarrierName();
            break;
            case 2: editHubName();
            break;
            case 3: editNodeName();
            break;
            case 4: menuDelegator.menuTransition(MAIN_MENU);
        }
    }

    @Override
    public String toString() {
        return this.getMenuType().getName();
    }

}
