package err.errigal.menu;
import com.google.gson.Gson;
import err.errigal.application.NetworkApplication;
import err.errigal.network_elements.AlarmType;

/*
 *  Purpose: Menu Manager for network application - Manages and delegates user actions to specific menu items
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public final class NetworkMenuDelegator implements IMenuDelegator {


    private Menu currentMenu;
    private final Menu mainMenu;
    private final Menu addMenu;
    private final Menu displayMenu;
    private final Menu removeMenu;
    private final Menu alarmMenu;
    private final Menu editMenu;
    private final Menu searchMenu;
    private final Menu statusMenu;
    private final NetworkApplication netApp;


    public NetworkMenuDelegator(NetworkApplication netApp, Gson gson) {
        this.netApp = netApp;
        mainMenu    = new MainMenu(this, netApp, gson);
        displayMenu = new DisplayMenu(this, netApp);
        removeMenu  = new RemoveMenu(this, netApp);
        addMenu     = new AddMenu(this, netApp);
        alarmMenu   = new AlarmMenu(this, netApp);
        editMenu    = new EditMenu(this, netApp);
        searchMenu  = new SearchMenu(this, netApp);
        statusMenu  = new StatusMenu(this, netApp);
        currentMenu = mainMenu;
    }

    public void displayMenuTitle() {
        System.out.println(currentMenu.getMenuTitle());
    }

    public void displayMenu() {
        System.out.println(currentMenu.getMenuDisplay());
    }

    public void processMenuAction() {
        currentMenu.handleMenuAction();
    }

    public void menuTransition(MenuType menuType) {

         switch(menuType) {
             case MAIN_MENU : currentMenu = mainMenu;
                break;
             case DISPLAY_MENU: currentMenu = displayMenu;
                 break;
             case REMOVE_MENU: currentMenu = removeMenu;
                 break;
             case SEARCH_MENU: currentMenu = searchMenu;
                break;
             case EDIT_MENU: currentMenu = editMenu;
                 break;
             case ALARM_MENU: currentMenu = alarmMenu;
                 break;
             case ADD_MENU: currentMenu = addMenu;
                break;
             case STATUS_MENU:currentMenu = statusMenu;
                break;
             case EXIT: netApp.setState(NetworkApplication.NetworkAppState.INACTIVE);
         }
    }

    public MenuType getCurrentMenuType() {
        return currentMenu.getMenuType();
    }

}
