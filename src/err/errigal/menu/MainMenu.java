package err.errigal.menu;
import com.google.gson.*;
import err.errigal.application.NetworkApplication;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.Network;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static err.errigal.menu.MenuType.*;


/*
 *  Purpose: Main Menu - Text based UI for main menu, display's all application options and handles importing/exporting networks
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class MainMenu extends Menu {

    private final IMenuDelegator menuDelegator;
    private final NetworkApplication netApp;
    private final Gson gson;

    private final String menuDisplay =  "1. Display Elements Menu           \t\t2. Display Element(s) Status\n"+
                                        "3. Add Network Element             \t\t4. Remove Network Element\n"+
                                        "5. Network Element Search          \t\t6. Edit Network Elements\n"+
                                        "7. Network Element Alarm Config    \t\t8. Export Network To JSON\n"+
                                        "9. Import Network From JSON        \t\t10. Exit\n";


    protected MainMenu(IMenuDelegator menuDelegator, NetworkApplication netApp, Gson gson) {
        super(MAIN_MENU, 10);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
        this.gson = gson;
    }

    @Override
    protected String getMenuDisplay() {
        return menuDisplay;
    }

    protected void processInput(int input) {
        switch(input) {
            case 1: menuDelegator.menuTransition(DISPLAY_MENU);
            break;
            case 2: menuDelegator.menuTransition(STATUS_MENU);
            break;
            case 3: menuDelegator.menuTransition(ADD_MENU);
            break;
            case 4: menuDelegator.menuTransition(REMOVE_MENU);
            break;
            case 5: menuDelegator.menuTransition(SEARCH_MENU);
            break;
            case 6: menuDelegator.menuTransition(EDIT_MENU);
            break;
            case 7: menuDelegator.menuTransition(ALARM_MENU);
            break;
            case 8: exportNetworkToJSON();
            break;
            case 9: importNetworkFromJSON();
            break;
            case 10: netApp.setState(NetworkApplication.NetworkAppState.INACTIVE);
        }
    }

    public void exportNetworkToJSON() {

        String path = InputHandler.readInputWithPrompt("Enter file path to network export to: ");
        Network network = netApp.getNetwork();

        try(FileWriter fw = new FileWriter(path)) {

            gson.toJson(network, fw);

        }catch (IOException e){
            System.out.println("Sorry could not export Network to JSON file, there was an error with your path:  ");
            return;
        }

        System.out.println("\n.....successfully export network to JSON to specified path: "+path+"\n");
    }

    public void importNetworkFromJSON() {

        String path = InputHandler.readInputWithPrompt("Enter file path of JSON network file to import : ");

        try(FileReader fr = new FileReader(path)) {

            Network network = gson.fromJson(fr, Network.class);

            if(network == null) {
                System.out.println("Sorry, there was an error importing network from JSON file at path: "+path);
                return;
            }
            else {
                netApp.setNetwork(network);
            }

        }catch (IOException e) {
           System.out.println("Sorry, there was an error importing network from JSON file at path: "+path);
           return;
        }

        System.out.println("\n.....successfully imported network from JSON at specified path: "+path+"\n");
    }

    @Override
    public String toString() {
        return this.getMenuType().getName();
    }
}


