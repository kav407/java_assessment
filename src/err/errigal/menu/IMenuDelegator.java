package err.errigal.menu;

public interface IMenuDelegator {

    void displayMenuTitle();
    void displayMenu();
    void processMenuAction();
    void menuTransition(MenuType menuType);
    MenuType getCurrentMenuType();
}
