package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.io.IOValidate;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.Carrier;
import err.errigal.network_elements.Hub;
import err.errigal.network_elements.Network;
import err.errigal.network_elements.Node;
import static err.errigal.menu.MenuType.*;
import static err.errigal.network_elements.Hub.*;
import static err.errigal.network_elements.Node.*;

/*
 *  Purpose: Text UI for adding components to application - Handles process of adding carriers, hubs and nodes to app
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public class AddMenu extends Menu {

    private final IMenuDelegator menuDelegator;
    private NetworkApplication netApp;

    private final String menuDisplay    =   "1. Add Carrier to Network      \t\t2. Add Hub to Carrier\n"+
                                            "3. Add Node to Hub             \t\t4. Exit to Main Menu\n";

    public AddMenu(IMenuDelegator menuDelegator, NetworkApplication netApp) {
        super(MenuType.ADD_MENU, 4);
        this.menuDelegator = menuDelegator;
        this.netApp = netApp;
    }

    public void addCarrier() {

        Network network = netApp.getNetwork();

        String carrierName = InputHandler.readInputWithPrompt("Name for new Carrier(Unique Per Network): ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Name for new Carrier(Unique Per Network):", "must be unique", true);

        if(carrierName.equals("-1")) {
            return;
        }

        int net_id = InputHandler.readInt("ID number for Carrier(Unique Per Network): ", "Invalid Input!!");
        net_id = IOValidate.validNetIDExists(network, net_id, "Enter ID number for Carrier, ", "Carrier IDs must be Unique Per Network!!\n");

        if(net_id == -1) {
            return;
        }

        Carrier carrier = new Carrier(net_id, carrierName);
        network.addCarrier(carrier);
        System.out.println("\n....successfully added Carrier "+carrierName+" to network\n");

    }

    public void addHub() {

        Network network = netApp.getNetwork();

        if(network.numOfCarriers() == 0) {
            System.out.println("\nNo carriers on Network to add hub to....\n");
            return;
        }

        String carrierName = InputHandler.readInputWithPrompt("Name of carrier For new hub: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Enter name Of carrier For new hub", "carrier does not exist", false);

        if(carrierName.equalsIgnoreCase("-1")) {
            return;   //User request to exit addHub
        }

        Carrier carrier = network.getCarrierByName(carrierName);

        String hubName = InputHandler.readInputWithPrompt("Name for new Hub(Unique Per Carrier): ");
        hubName = IOValidate.validHubName(carrier, hubName, "Name for new Hub(Unique Per Carrier): ","must be unique", true);

        if(hubName.equals("-1")) {
            return;      //User request to exit addHub
        }

        int net_id = InputHandler.readInt("Enter ID number for Hub: ", "Invalid Input!!");
        net_id = IOValidate.validNetIDExists(network, net_id, "Enter ID number for Hub ", "Carrier & Net IDs must be Unique!!!");

        if(net_id == -1) {
            return;     //User request to exit addHub
        }

        //Add hub to to the networks carrier
        Hub hub = new HubBuilder(net_id,hubName).build();
        network.addHubToCarrier(carrierName, hub);
        System.out.println("\n....successfully added Hub "+hubName+" to Carrier "+carrierName+"\n");
    }

    public void addNode() {

        Network network = netApp.getNetwork();

        if(network.numOfHubs() == 0) {
            System.out.println("\nNo Hubs on Network, therefore no hubs to add node to....\n");
            return;
        }

        String carrierName = InputHandler.readInputWithPrompt("Name Of Carrier For New Node: ");
        carrierName = IOValidate.validCarrierName(network, carrierName, "Name Of Carrier For New Node: ", "carrier does not exist", false);

        if(carrierName.equals("-1")) {
            return;   //User request to exit
        }

        Carrier carrier = network.getCarrierByName(carrierName);

        if(carrier.getHubList().size() == 0) {
            System.out.println("\nNo Hubs on this Carrier, please add hub first before adding node....\n");
            return;
        }

        String hubName = InputHandler.readInputWithPrompt("Name Of Hub to add New Node: ");
        hubName = IOValidate.validHubName(carrier, hubName, "Name Of Hub to add New Node: ", "hub does not exist", false);
        if(hubName.equals("-1")) {
            return;   //User request to exit
        }

        Hub hub = carrier.getHubByName(hubName);

        String nodeName = InputHandler.readInputWithPrompt("Name for new Node(Unique Per Hub): ");
        nodeName = IOValidate.validNodeName(hub, nodeName, "Name for new Node(Unique Per Hub): ","must be unique", true);
        if(nodeName.equals("-1")) {
            return;       //User request to exit
        }

        int net_id = InputHandler.readInt("ID number for Node: ", "Invalid Input!!");
        net_id = IOValidate.validNetIDExists(network, net_id, "Enter ID number for Node ", "Carrier & Net IDs must be Unique!!");

        if(net_id == -1) {
            return;       //User request to exit addHub
        }

        Node node = new NodeBuilder(net_id, nodeName).build();
        network.addNodeToHub(carrierName,hubName, node);
        System.out.println("\n....successfully added Node "+nodeName+" to Hub "+hubName+" on Carrier "+carrierName+"\n");
    }

    @Override
    public String toString() {
        return this.getMenuType().getName();
    }

    @Override
    public String getMenuDisplay() {
        return menuDisplay;
    }

    protected void processInput(int option) {
        switch(option) {
            case 1: addCarrier();
            break;
            case 2: addHub();
            break;
            case 3: addNode();
            break;
            case 4: menuDelegator.menuTransition(MAIN_MENU);
        }
    }
}
