package err.errigal.io;
import err.errigal.network_elements.Alarm;
import err.errigal.network_elements.Carrier;
import err.errigal.network_elements.Hub;
import err.errigal.network_elements.Network;

/*
 *  Purpose: User Input Validator - Validates user input in terms of network application
 *  Written by Andrew Kavanagh <andrew.kavanagh@errigal.com>, 17 August 2017
 */

public final class IOValidate {

    private IOValidate(){
        throw new UnsupportedOperationException("IOValidate cannot be initialized....");
    }

    public static boolean integerInRange(int i, int max) {
        return i>=1 && i<=max;
    }

    public static boolean integerInRange(int i, int max, String errMsg) {
        if(i>=1 && i<=max) {
            return true;
        }
        System.out.println(errMsg);
        return false;
    }

    public static int indexInRange(int min, int max, String prompt, String errMsg) {
        int val = min-1;

        while(val<min || val >max) {

            val = InputHandler.readInt(prompt, "not a valid input!!!");

            if(val < min || val >max) {
                System.out.println(errMsg);
            }

            if(val == -1) {
                return -1;
            }
        }

        return val;
    }

    public static String validCarrierName(Network network, String carrierName, String prompt, String err, boolean exists) {

        boolean nameUnique = network.carrierNameExists(carrierName);

        while(nameUnique == exists) {

            System.out.println(carrierName+" is not a valid carrier name, "+err+".");
            carrierName = InputHandler.readInputWithPrompt(prompt+", or press -1 to exit: ");

            if(carrierName.equalsIgnoreCase("-1")) {
                break;
            }

            nameUnique = network.carrierNameExists(carrierName);
        }

        return carrierName;
    }

    public static String validCarrierID(Network network, String carrierID, String prompt, String err, boolean exists) {

        boolean idUnique = network.carrierIDExists(Integer.parseInt(carrierID));

        while(idUnique == exists) {

            System.out.println(err);
            carrierID = InputHandler.readInputWithPrompt(prompt+", or type exit to quit: ");

            if(carrierID.equalsIgnoreCase("exit")) {
                break;
            }

            if(!carrierID.matches("\\d+") || !network.carrierIDExists(Integer.parseInt(carrierID))) {
                continue;
            }

            idUnique = network.carrierIDExists(Integer.parseInt(carrierID));
        }

        return carrierID;
    }

    public static String validHubName(Carrier carrier, String hubName, String prompt, String err, boolean exists) {

        boolean nameExists = carrier.hubExists(hubName);

        while(nameExists == exists) {

            System.out.println(hubName+" is not a valid hub name, "+err+", or press -1 to exit: ");
            hubName = InputHandler.readInputWithPrompt(prompt);

            if(hubName.equalsIgnoreCase("-1")) {
                break;
            }

            nameExists = carrier.hubExists(hubName);
        }
        return hubName;
    }

    public static String validNodeName(Hub hub, String nodeName, String prompt, String err, boolean exists) {

        boolean nameExists = hub.nodeExists(nodeName);

        while(nameExists == exists) {

            System.out.println(nodeName+" is not a valid node name, "+err+", or press -1 to exit: ");
            nodeName = InputHandler.readInputWithPrompt(prompt);

            if(nodeName.equalsIgnoreCase("-1")) {
                break;
            }

            nameExists = hub.nodeExists(nodeName);
        }

        return nodeName;
    }

    public static int validNetIDExists(Network network, int net_id, String prompt, String err) {

        boolean id_exits = network.netIDExists(net_id);
        id_exits |= network.carrierIDExists(net_id);

        while(id_exits || net_id == -1) {

            if(id_exits) {
                System.out.println(err);
            }

            net_id = InputHandler.readInt(prompt+"or press -1 to exit: ", "Invalid input");
            id_exits = network.netIDExists(net_id);
            id_exits |= network.carrierIDExists(net_id);

            if(net_id == -1) {
                break;
            }
        }

        return net_id;
    }

    public static int validNetIDNotExists(Network network, int net_id, String prompt, String err) {

        boolean id_exits = network.netIDExists(net_id);

        while(!id_exits || net_id == -1) {

            if(!id_exits) {
                System.out.println(err);
            }

            net_id = InputHandler.readInt(prompt+"or press -1 to exit: ", "Invalid input");
            id_exits = network.netIDExists(net_id);

            if(net_id == -1) {
                break;
            }
        }

        return net_id;
    }

    public static int validAlarmId(String prompt, String errMsg) {

        int id = InputHandler.readInt(prompt, errMsg);

        while(Alarm.alarmIDExists(id)) {

            if(Alarm.alarmIDExists(id)) {
                System.out.println(errMsg);
            }

            id = InputHandler.readInt(prompt, errMsg);

            if(id == -1) {
                return -1;
            }
        }

        return id;
    }
}
