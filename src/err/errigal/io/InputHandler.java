package err.errigal.io;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Author: Andrew Kavanagh.
 *
 * Purpose: Handle input within the Address Book application.
 * Notes  : Used when adding default contacts to address book
 */
public final class InputHandler {

    private static Scanner in = new Scanner(System.in);

    private InputHandler() {
        throw new UnsupportedOperationException("InputHandler cannot be initialized....");
    }

    public static String readInputWithPrompt(String prompt) {
        System.out.print(prompt);
        String input = in.nextLine();
        return input;
    }

    //Reads input until valid int value provided by System in
    public static int readInt(String prompt, String err) {
        try {
            System.out.print(prompt);
            String str = in.nextLine();
            /*Stops junk newline being left in buffer caused by nextInt*/
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            System.out.println(err);
            return -2;
        }

    }

    public static void setInputStream(InputStream inStream) {
        in = new Scanner(inStream);
    }

    public static void close() {
        in.close();
    }
}