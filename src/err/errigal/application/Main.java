package err.errigal.application;

public class Main {

    public static void main(String [] args) {

        /* JSON network can be passed from args*/
        Application networkApp = (args.length == 1)?new NetworkApplication(args[0]) : new NetworkApplication();
        networkApp.run();
    }
}
