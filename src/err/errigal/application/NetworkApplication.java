package err.errigal.application;
import com.google.gson.*;
import err.errigal.io.Adaptor;
import err.errigal.io.InputHandler;
import err.errigal.menu.IMenuDelegator;
import err.errigal.menu.NetworkMenuDelegator;
import err.errigal.network_elements.Network;
import err.errigal.network_elements.NetworkElement;
import err.errigal.patternInterfaces.Observable;
import err.errigal.patternInterfaces.Observer;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;

public class NetworkApplication implements Application {

    private NetworkAppState state;
    private IMenuDelegator menuDelegator;
    private Network network;
    private final Gson gson;

    public enum NetworkAppState {
        ACTIVE, INACTIVE
    }

    public NetworkApplication() {
        GsonBuilder builder = new GsonBuilder().registerTypeAdapter(NetworkElement.class, new Adaptor());
        builder.registerTypeAdapter(Observer.class, new Adaptor());
        gson = builder.registerTypeAdapter(Observable.class, new Adaptor()).setPrettyPrinting().create();
        state = NetworkAppState.ACTIVE;
        network = new Network();
        menuDelegator = new NetworkMenuDelegator(this, gson);
        network = new Network();

    }

    public NetworkApplication(String networkJSON) {
        GsonBuilder builder = new GsonBuilder().registerTypeAdapter(NetworkElement.class, new Adaptor());
        builder.registerTypeAdapter(Observer.class, new Adaptor());
        gson = builder.registerTypeAdapter(Observable.class, new Adaptor()).setPrettyPrinting().create();
        state = NetworkAppState.ACTIVE;
        menuDelegator = new NetworkMenuDelegator(this, gson);
        network = buildNetwork(networkJSON);
    }

    public void run() {

        while(state == NetworkAppState.ACTIVE) {
            menuDelegator.displayMenuTitle();
            menuDelegator.displayMenu();
            menuDelegator.processMenuAction();
        }

        cleanUp();
    }

    public NetworkAppState getState() {
        return state;
    }

    private final Network buildNetwork(String jsonPath) {

        try(FileReader fr = new FileReader(jsonPath)) {

            Network newNet = gson.fromJson(fr, Network.class);

            if(newNet == null) {
                System.out.println("Sorry, <args> json network was not imported.....");
                return new Network();
            }
            else {
                return new Network(newNet); //reconstruct network correctly after deserialization using copy cons
            }

        }catch (IOException e) {
            System.out.println("Sorry, <args> jsonNetwork was not imported.....");
            return new Network();
        }
    }

    public void setState(NetworkAppState state) {
        this.state = state;
    }

    private void cleanUp() {
        InputHandler.close();
    }

    @Override
    public String toString() {
        return "Network Application";
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        Network newNetwork = new Network(network);
        this.network = newNetwork;
    }
}