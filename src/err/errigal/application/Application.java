package err.errigal.application;

/**
 * Author: Andrew Kavanagh
 *
 * Purpose: Serve a common base class for menu's. Ensures
 *          derived menu's provide the required behaviour.
 */

public interface Application {
    void run();
}
