package err.errigal.sorting_searching;

/* Used to keep network elements list sorted
   offers roughly O(N) as will always
   be called on a nearly fully sorted
   list i.e it will be called adding one element
   to a sorted list. **Currently not used***
*/

import java.util.Collections;
import java.util.List;

public class InsertionSort<T extends Comparable> {

    public void sort(List<T> elements) {
        for(int i=1; i<elements.size(); i++) {
            int j = i;
            while(j>0 && elements.get(j-1).compareTo(elements.get(j))>0) {
                Collections.swap(elements, j-1, j);
                j--;
            }
        }
    }
}
