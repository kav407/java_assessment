package err.errigal.sorting_searching;
import err.errigal.network_elements.Network;
import err.errigal.network_elements.NetworkElement;

import java.util.List;

/**
 * Author: Andrew Kavanagh.
 *
 * Purpose: Utility class, modified version of Binary search that find first/last occurrence of a string
 * Notes  : Useful for searching Sorted Address Book, where duplicate can be present
 */
public class ModifiedBinarySearch<T extends NetworkElement>{

    //Gets the first occurrence of specified string, return -1 if not found
    public int getFirstOccurrenceString(List<T> list, String element) {
        int res = -1;
        int low = 0;
        int high = list.size()-1;

        while(low <=high) {
            int mid = low+(high-low)/2;

            if((element.compareToIgnoreCase(list.get(mid).getName()) == 0)) {
                res = mid;
                high = mid - 1;
            }
            else if(element.compareToIgnoreCase(list.get(mid).getName()) < 0)
                high = mid-1;
            else
                low = mid+1;
        }
        return res;
    }

    //Gets the last occurrence of specified string, return -1 if not found
    public int getLastOccurrenceString(List<T> list,  String element) {
        int res = -1;
        int low = 0;
        int high = list.size()-1;

        while(low <=high) {
            int mid = low+(high-low)/2;

            if((element.compareToIgnoreCase(list.get(mid).getName()) == 0)) {
                res = mid;
                low = mid + 1;
            }
            else if(element.compareToIgnoreCase(list.get(mid).getName()) < 0)
                high = mid-1;
            else
                low = mid+1;
        }

        return res;
    }
}
