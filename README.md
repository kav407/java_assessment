#Dependancies
* **Junit 4**
* **Gson 2.8.1**

**Gson is included in the project needs to be added as lib to class path**

**Command Line Args(Optional)** - To start application with a populated network pass 
						“src/sample_network.json” as a command line arg.
						 If no arg passed, a default network with no components will be used by the app.

**OR**: To test import/export with JSON, the application can be loaded with no args and the same network in src can be imported using the options in the main menu.