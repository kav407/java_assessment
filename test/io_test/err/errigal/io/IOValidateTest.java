package err.errigal.io;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.*;

public class IOValidateTest {

    @Test(expected = UnsupportedOperationException.class)
    public void illegalInstantiationTest() throws Exception {
        Constructor<IOValidate> cons = IOValidate.class.getDeclaredConstructor();
        cons.setAccessible(true);

        try {
            IOValidate newInstance = cons.newInstance();
        } catch (InvocationTargetException e) {
            if(e.getCause() instanceof UnsupportedOperationException) {
                throw (UnsupportedOperationException)e.getCause();
            }
        }
    }
    @Test
    public void integerInRange() throws Exception {
        //Test a range
        for(int i = 1; i<11; i++) {
            assertTrue(IOValidate.integerInRange(i, 10));
        }
    }

    @Test
    public void integerInRangeFail() throws Exception {

        //Test outside of lower bound
        assertFalse(IOValidate.integerInRange(0,10));
        //Test outside of upper bound
        assertFalse(IOValidate.integerInRange(11,10));

    }

    @Test
    public void integerInRangePromptTest() throws Exception {
        //Test a range
        for(int i = 1; i<11; i++) {
            assertTrue(IOValidate.integerInRange(i, 10, "Prompt"));
        }
    }

    @Test
    public void integerInRangeFailPromptTest() throws Exception {

        //Test outside of lower bound
        assertFalse(IOValidate.integerInRange(0, 10, "Prompt"));
        //Test outside of upper bound
        assertFalse(IOValidate.integerInRange(11, 10,"Prompt"));

    }

}