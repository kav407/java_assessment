package err.errigal.io;

import org.junit.*;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.*;

public class InputHandlerTest {

    private static ByteArrayOutputStream outStream;
    private static PrintStream stdOut;

    @BeforeClass
    public static void beforeClass() {
        outStream = new ByteArrayOutputStream();
        stdOut = System.out;
    }

    @AfterClass //ensure stdin restore after runner completes
    public static void afterClass() {
        System.setOut(stdOut);
    }


    @Before
    public void setUpStream() {
        outStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outStream));
    }

    @After
    public void cleanUpStream() {
        try {
            outStream.close();                                  //close temp output stream
        } catch (IOException e ) { e.printStackTrace(); }

        System.setOut(stdOut);                                   //set back to normal stdin
        InputHandler.close();                                   //close input stream
    }

    //Test illegal instantiation of Input Handler
    @Test(expected = UnsupportedOperationException.class)
    public void illegalInstantiationTest() throws Exception {

        Constructor<InputHandler> inputHandlerCons = null;
        inputHandlerCons = InputHandler.class.getDeclaredConstructor();
        inputHandlerCons.setAccessible(true);

        try {
            InputHandler newInstanceInputHandler = inputHandlerCons.newInstance();
        }catch (InvocationTargetException e) {
            if(e.getCause() instanceof UnsupportedOperationException) {
                throw (UnsupportedOperationException)e.getCause();
            }
        }
    }

    @Test
    public void readInputWithPromptTest() throws Exception {

        String expectedOutput = "Enter the capital of France";
        String givenInput = "Paris";

        InputHandler.setInputStream(new ByteArrayInputStream(givenInput.getBytes()));
        String receivedInput = InputHandler.readInputWithPrompt("Enter the capital of France");

        assertEquals(givenInput, receivedInput);                    //Check Input correctly recorded
        assertEquals(expectedOutput, outStream.toString());         //Test expected output
    }


    @Test
    public void readIntTest() {
        InputHandler.setInputStream(new ByteArrayInputStream("6".getBytes()));
        int expected = 6;
        int givenValue = InputHandler.readInt("Enter the number that comes after 5", "Error");
        assertEquals(expected, givenValue);
    }

    @Test
    public void readIntTestWithError() {
        InputHandler.setInputStream(new ByteArrayInputStream("sdf".getBytes()));
        int givenValue = InputHandler.readInt("Enter the number that comes after 5", "Error");
        int expectedFail = -2;
        assertEquals(expectedFail, givenValue);
    }


    @Test
    public void setInputStream() {
        InputStream in = new ByteArrayInputStream("Writing to my stream".getBytes());
        InputHandler.setInputStream(in);
        String result = InputHandler.readInputWithPrompt("");
        String expected = "Writing to my stream";
        assertEquals(expected, result);

    }

    @Test(expected = IOException.class)
    public void closeTest() throws IOException {
        InputStream test = new ByteArrayInputStream("dsdasd".getBytes());
        InputHandler.setInputStream(System.in);
        InputHandler.close();

        System.in.read();
    }
}