package err.errigal.network_elements;
import com.google.gson.Gson;
import err.errigal.application.NetworkApplication;
import err.errigal.io.InputHandler;
import org.junit.*;


import java.io.*;

import static org.junit.Assert.*;
import static err.errigal.application.NetworkApplication.NetworkAppState.*;
public class NetworkManagerTest {

    private NetworkApplication netAppManager;
    private static InputStream stdIn;
    private static PrintStream stdOut;
    private ByteArrayOutputStream stdOutCopy;
    private static String delim;


    @BeforeClass
    public static void beforeClass() {
        stdIn = System.in;
        stdOut = System.out;
        delim = System.getProperty("line.separator");
    }

    @AfterClass
    public static void afterClass() {
        System.setIn(stdIn);
        System.setOut(stdOut);
    }

    @Before
    public void beforeEachTest() {
        netAppManager = new NetworkApplication();
        stdOutCopy = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdOutCopy));

    }

    @After
    public void afterEachTest() {
        netAppManager = null;
        System.setOut(null);
        System.setIn(null);
        System.setIn(stdIn);
        System.setOut(stdOut);
    }

    @Test
    public void runTest() {

        String userInputMock = "1"+delim+"8"+delim+"10";                                        //Press menu option and quit
        InputHandler.setInputStream(new ByteArrayInputStream(userInputMock.getBytes()));        //Mock user input
        netAppManager.run();                                                                    //Run the app
        assertEquals(netAppManager.getState(), INACTIVE);                                       //Assert the app started

    }

    @Test
    public void toStringTest() {

        NetworkApplication netAppManager = new NetworkApplication();

        String expected = "Network Application";
        String actual = netAppManager.toString();

        assertEquals(actual, expected);
    }

    @Test
    public void getStateTest() {
        NetworkApplication netAppManager = new NetworkApplication();
        assertEquals(netAppManager.getState(), ACTIVE);
    }
}