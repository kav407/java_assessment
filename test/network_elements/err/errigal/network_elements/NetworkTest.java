package err.errigal.network_elements;

import err.errigal.network_elements.Node.NodeBuilder;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static err.errigal.network_elements.Hub.*;
import static err.errigal.network_elements.Result.*;
import static org.junit.Assert.*;

public class NetworkTest {
    @Test
    public void addCarrierTest() throws Exception {

        Network network = new Network();
        Carrier vodaFone = new Carrier(1,"VODAFONE");

        //Test Null Add
         assertEquals(NULL_VALUE_GIVEN, network.addCarrier(null));

        //Test non unique carrier name
        network.addCarrier(vodaFone);
        assertEquals(NON_UNIQUE_CARRIER_NAME, network.addCarrier(new Carrier(2, "VODAFONE")));

        /*
            Ensure carrier added without children, carriers must be solely added
            and children added at to that carrier through network to ensure integrity with network identifiers
        */

        Carrier three = new Carrier(3, "THREE");
        three.addHub(new HubBuilder(12, "H_001").build());
        assertEquals(CARRIER_CONTAINS_CHILDREN, network.addCarrier(three));
    }

    @Test
    public void addHubTest() throws Exception {

        Network network = new Network();
        network.addCarrier(new Carrier(1,"VODAFONE"));

        //Null carrier name passed
        assertEquals(NULL_VALUE_GIVEN, network.addHubToCarrier(null, new HubBuilder(3, "H_001").build()));

        //Carrier does not exist or has not been added to network
        assertEquals(CARRIER_NOT_FOUND, network.addHubToCarrier("THREE", new HubBuilder(3, "H_001").build()));

        //Duplicate Network Identifier
        network.addHubToCarrier("VODAFONE", new HubBuilder(3, "H_001").build());
        assertEquals(NON_UNIQUE_ID, network.addHubToCarrier("VODAFONE", new HubBuilder(3, "H_120").build()));

        //Not unique hub names
        assertEquals(NON_UNIQUE_HUB_NAME, network.addHubToCarrier("VODAFONE", new HubBuilder(34, "H_001").build()));

        //Assert Hub has no children
        Hub hub = new HubBuilder(123,"H_678").build();
        hub.addNode(new NodeBuilder(45,"N_001").build());
        network.addHubToCarrier("VODAFONE", hub);
        assertEquals(HUB_CONTAINS_CHILDREN, network.addHubToCarrier("VODAFONE", hub));
    }

    @Test
    public void addNodeTest() throws Exception {

        Network network = new Network();
        network.addCarrier(new Carrier(1,"VODAFONE"));
        network.addHubToCarrier("VODAFONE", new HubBuilder(34,"H_001").build());

        //Test against null
        assertEquals(NULL_VALUE_GIVEN, network.addNodeToHub("VODAFONE", "H_001", null));

        //Test against carrier not existing
        assertEquals(CARRIER_NOT_FOUND, network.addNodeToHub("THREE", "H_001", new NodeBuilder(56, "N_089").build()));

        //Test against hub not found or does not exist
        assertEquals(HUB_NOT_FOUND, network.addNodeToHub("VODAFONE", "H_111", new NodeBuilder(56, "N_089").build()));

        //Test against non unique node name
        network.addNodeToHub("VODAFONE", "H_001", new NodeBuilder(56, "N_089").build());
        assertEquals(NON_UNIQUE_NODE_NAME, network.addNodeToHub("VODAFONE", "H_001", new NodeBuilder(56, "N_089").build()));

        //Test against non unique  network identifer
        assertEquals(NON_UNIQUE_ID, network.addNodeToHub("VODAFONE", "H_001", new NodeBuilder(56, "N_100").build())); //same id as node added
        assertEquals(NON_UNIQUE_ID, network.addNodeToHub("VODAFONE", "H_001", new NodeBuilder(34, "N_100").build())); //same id as hub on network
    }

    @Test
    public void getNetworkTestCarriersTest() throws Exception {

        Network network = new Network();
        Carrier vodaFone = new Carrier(1,"VODAFONE");
        Carrier three = new Carrier(2,"THREE");
        Carrier o2 = new Carrier(3,"O2");

        network.addCarrier(vodaFone);
        network.addCarrier(three);
        network.addCarrier(o2);

        Hub vodaHub1 = new HubBuilder(12, "H_001").ipAddress("212.12.90.0").build();

        network.addHubToCarrier("VODAFONE", vodaHub1);
        Node vodaNode1 = new NodeBuilder(45, "N_002").ipAddress("212.12.90.1").build();
        Node vodaNode2 = new NodeBuilder(33, "N_003").ipAddress("212.12.90.2").build();
        Node vodaNode3 = new NodeBuilder(56, "N_004").ipAddress("212.12.90.3").build();

        network.addNodeToHub("VODAFONE", "H_001", vodaNode1);
        network.addNodeToHub("VODAFONE", "H_001", vodaNode2);
        network.addNodeToHub("VODAFONE","H_001", vodaNode3);

        List<Carrier> list = new ArrayList();
        list.add(vodaFone);
        list.add(three);
        list.add(o2);
        
        assertEquals(list, network.getNetworkCarriers());

    }

}