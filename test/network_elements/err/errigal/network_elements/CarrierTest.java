package err.errigal.network_elements;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import static err.errigal.network_elements.Hub.*;
import static org.junit.Assert.*;

public class CarrierTest {

    private Carrier carrier;
    private HubBuilder hubBuilder;
    private static ByteArrayOutputStream mockOutput;
    private static PrintStream stdOut;

    @BeforeClass
    public static void beforeClass() {
        stdOut = System.out;
    }

    @AfterClass
    public static void afterClass() throws IOException{
        System.setOut(stdOut);
        mockOutput.close();
    }

    @Before
    public void beforeTest() {
        carrier = new Carrier(45, "VodaFone");
        hubBuilder = new HubBuilder(123, "H_234");
        mockOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(mockOutput));
    }
    @Test
    public void addHubYTest() {
        Hub hub = hubBuilder.build();
        boolean op1 = carrier.addHub(hub);
        boolean op2 = carrier.addHub(hub); //try add same hub twice
        assertTrue(op1);
        assertFalse(op2);
        assertEquals(carrier.getId(), hub.getParentID());
    }

    @Test
    public void displayChildrenTest() {
        Hub hub1 = new HubBuilder(12, "R_002").build();
        Hub hub2 = new HubBuilder(13, "R_003").build();
        Hub hub3 = new HubBuilder(14, "R_004").build();

        carrier.addHub(hub1);
        carrier.addHub(hub2);
        carrier.addHub(hub3);

        StringBuilder expected = new StringBuilder();
        expected.append("\t\t-> Hub "+hub1+"\n");
        expected.append("\t\t-> Hub "+hub2+"\n");
        expected.append("\t\t-> Hub "+hub3+"\n");

        carrier.displayChildren();

        assertEquals(expected.toString(), mockOutput.toString());
    }

    @Test
    public void getHubsTest() {
        Hub hub1 = new HubBuilder(12, "R_002").build();
        Hub hub2 = new HubBuilder(13, "R_003").build();
        Hub hub3 = new HubBuilder(14, "R_004").build();

        carrier.addHub(hub1);
        carrier.addHub(hub2);
        carrier.addHub(hub3);

        List<Hub> returnedHubs = carrier.getHubList();

        assertEquals(hub1, returnedHubs.get(0));
        assertTrue(hub2.equals(returnedHubs.get(1)));
        assertTrue(hub3.equals(returnedHubs.get(2)));
    }

    @Test
    public void addChildHubTest() {
        Hub hub1 = new HubBuilder(12, "R_002").build();
        Hub hub2 = new HubBuilder(13, "R_003").build();
        Hub hub3 = new HubBuilder(14, "R_004").build();

        //Null check
        assertFalse(carrier.addHub(null));

        //Non uniqiue hub name add check
        assertTrue(carrier.addHub(hub1));
        assertFalse(carrier.addHub(hub1));

    }

    @Test
    public void getNameTest(){
        String expected = "VodaFone";
        assertEquals(expected, carrier.getName());
    }
    @Test
    public void toStringTest() {
        carrier.addHub(new HubBuilder(12, "R_002").build());
        String expected = "VodaFone, ID: 45, Net Child Elements: 1, Net GrandChild Elements: 0";
        assertEquals(expected, carrier.toString());
    }

}