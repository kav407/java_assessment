package err.errigal.network_elements;
import static err.errigal.network_elements.Alarm.AlarmBuilder;
import static err.errigal.network_elements.AlarmType.Status.*;
import static err.errigal.network_elements.Node.NodeBuilder;
import static err.errigal.network_elements.Hub.HubBuilder;
import static err.errigal.network_elements.AlarmType.*;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.TestCase.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.io.PrintStream;
import java.util.*;
import org.junit.*;

public class HubTest {

    private static PrintStream stdOut;
    private static ByteArrayOutputStream mockOutPut;
    private HubBuilder hubBuilder;
    private NodeBuilder nodeBuilder;

    @BeforeClass
    public static void beforeClass() {
        stdOut = System.out;
    }

    @Before
    public void beforeTest() {
        hubBuilder = new HubBuilder(123, "H_234");
        nodeBuilder = new NodeBuilder(345, "R_001");
        mockOutPut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(mockOutPut));
    }

    @After
    public void afterTest() throws IOException{
        System.setOut(stdOut);
        mockOutPut.close();
    }

    @Test
    public void hubWithBuilderTest() {
        //Create Hub Using Builder
        hubBuilder = hubBuilder.ipAddress("127.0.0.1");
        hubBuilder = hubBuilder.parentId(345);
        hubBuilder = hubBuilder.description("Important Hub Description");
        Hub newHub = hubBuilder.build();

        assertEquals(123, newHub.getID());
        assertEquals("127.0.0.1", newHub.getIP());
        assertEquals(NeType.HUB, newHub.getType());
        assertEquals(345, newHub.getParentID());
        assertEquals("Important Hub Description", newHub.getDescription());
    }

    @Test
    public void copyHubWithBuilderTest() {
        //Create Hub Using Builder
        hubBuilder = hubBuilder.ipAddress("127.0.0.1");
        hubBuilder = hubBuilder.parentId(12);
        hubBuilder = hubBuilder.description("Important Hub Description");

        Hub newHub = hubBuilder.build();

        newHub.addNode(new NodeBuilder(12,"R_0045").build());
        newHub.addNode(new NodeBuilder(13,"R_0046").build());
        newHub.addNode(new NodeBuilder(14,"R_0047").build());

        Hub copyHub = new HubBuilder(newHub).build();

        //Confirm Shallow Copy
        assertEquals(newHub.getID(), copyHub.getID());                      //primitive int id
        assertEquals(newHub.getIP(), copyHub.getIP());        //immutable string IP
        assertEquals(newHub.getParentID(), copyHub.getParentID());          //primitive int parent id
        assertEquals(newHub.getDescription(), copyHub.getDescription());    //immutable string description


        assertNotSame(newHub.getNodes(), copyHub.getNodes());         //Children List Objects copied(different references)

        //Children elements same values
        assertEquals(newHub.getNodes().get(0).toString(), copyHub.getNodes().get(0).toString());
        assertEquals(newHub.getNodes().get(1).toString(), copyHub.getNodes().get(1).toString());
        assertEquals(newHub.getNodes().get(2).toString(), copyHub.getNodes().get(2).toString());
    }

    @Test
    public void getIDTest() {
        Hub hub = hubBuilder.build();
        assertEquals(123, hub.getID());
    }

    @Test
    public void getTypeTest() {
        Hub hub = hubBuilder.build();
        assertEquals("HUB", hub.getType().toString());
    }

    @Test //Revise later
    public void displayChildrenTest() {
        Hub hub = hubBuilder.build();
        Node node1 = new NodeBuilder(12, "R_002").build();
        Node node2 = new NodeBuilder(13, "R_003").build();
        Node node3 = new NodeBuilder(14, "R_004").build();

        hub.addNode(node1);
        hub.addNode(node2);
        hub.addNode(node3);

        StringBuilder expected = new StringBuilder();
        expected.append("\t\t\t->"+node1+"\n");
        expected.append("\t\t\t->"+node2+"\n");
        expected.append("\t\t\t->"+node3+"\n");

        hub.displayChildren();

        assertEquals(expected.toString(), mockOutPut.toString());
    }

    @Test
    public void toStringTest() {
        Hub newHub = hubBuilder.build();
        String expected = "NET ID: "+newHub.getID()+", Type: HUB, NET ID: 123, Name: H_234, IP_ADDRESS: , PARENT_ID: -1, DESC: Not Provided, Active Alarm Count: 0, CURRENT STATE: ONLINE";
        assertEquals(expected, newHub.toString());
    }
    
    @Test
    public void addChildNodeTest() {
        //Check add null
        Hub hub = hubBuilder.build();
        hub.addNode(null);
        assertEquals(0, hub.getNodes().size());


        Node testNode1 = nodeBuilder.build();
        Node testNode2 = new NodeBuilder(345,"R_001").build(); //same name as test node 1
        Node testNode3 = new NodeBuilder(345,"R_045").build(); //same name as test node 1 different name
        boolean op1 = hub.addNode(testNode1);
        boolean op2 = hub.addNode(testNode2);

        //Check cant add nodes with same name
        assertTrue(op1);
        assertFalse(op2);

        //Assert node not replaced
        assertEquals(testNode1, hub.getNodes().get(0));

    }
    @Test
    public void setIPAddressTest() {
        Hub hub = hubBuilder.build();
        String expected  = "200.54.140.0";
        hub.setIpAddress("200.54.140.0");
        assertEquals(expected, hub.getIP());
    }

    @Test
    public void addAlarmTest() {
        Hub hub = hubBuilder.build();
        Alarm powerOutage = new AlarmBuilder(12,LocalDateTime.now(), POWER_OUTAGE).build();
        Alarm darkFibre = new AlarmBuilder(12, LocalDateTime.now(), DARK_FIBRE).build();

        boolean op1 = hub.addAlarm(powerOutage);
        boolean op2 = hub.addAlarm(darkFibre);

        //assert adds successful
        assertTrue(op1);
        assertTrue(op2);

        //Confirm alarms added correctly
        assertTrue(powerOutage.equals(hub.getAlarmByType(POWER_OUTAGE).get(0)));
        assertTrue(darkFibre.equals(hub.getAlarmByType(DARK_FIBRE).get(0)));

        //Try add duplicate alarm
        boolean op3 = hub.addAlarm(powerOutage);
        assertFalse(op3);
    }

    @Test
    public void setDescriptionTest() {
        Hub hub = hubBuilder.description("Testing Description").build();
        hub.setDescription("Edited Description");
        String expected = "Edited Description";
        assertEquals(expected, hub.getDescription());
    }

    @Test
    public void getAlarmByTypeTest() {
        Hub hub = hubBuilder.build();


        //Power Outage alarms
        Alarm powerOutage1 = new AlarmBuilder(12,LocalDateTime.now(), POWER_OUTAGE).currentStatus(MEDIUM).build();
        Alarm powerOutage2 = new AlarmBuilder(13,LocalDateTime.now(), POWER_OUTAGE).currentStatus(MEDIUM).build();
        Alarm powerOutage3 = new AlarmBuilder(14,LocalDateTime.now(), POWER_OUTAGE).currentStatus(MEDIUM).build();

        //Dark Fibre Alarms
        Alarm darkFibre1 = new AlarmBuilder(15,LocalDateTime.now(), DARK_FIBRE).currentStatus(MINOR).build();
        Alarm darkFibre2 = new AlarmBuilder(16,LocalDateTime.now(), DARK_FIBRE).currentStatus(MINOR).build();
        Alarm darkFibre3 = new AlarmBuilder(17,LocalDateTime.now(), DARK_FIBRE).currentStatus(MINOR).build();

        //Unit Unavailable Alarms
        Alarm unavail1 = new AlarmBuilder(18,LocalDateTime.now(), UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();
        Alarm unavail2 = new AlarmBuilder(19,LocalDateTime.now(), UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();
        Alarm unavail3 = new AlarmBuilder(20,LocalDateTime.now(), UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();

        //Optical Fibre Loss Alarms
        Alarm fibreLoss1 = new AlarmBuilder(21,LocalDateTime.now(), OPTICAL_FIBRE_LOSS).build();
        Alarm fibreLoss2 = new AlarmBuilder(22,LocalDateTime.now(), OPTICAL_FIBRE_LOSS).build();
        Alarm fibreLoss3 = new AlarmBuilder(23, LocalDateTime.now(), OPTICAL_FIBRE_LOSS).build();

        /* add all alarm types to hub*/
        hub.addAlarm(powerOutage1);
        hub.addAlarm(powerOutage2);
        hub.addAlarm(powerOutage3);

        hub.addAlarm(darkFibre1);
        hub.addAlarm(darkFibre2);
        hub.addAlarm(darkFibre3);

        hub.addAlarm(unavail1);
        hub.addAlarm(unavail2);
        hub.addAlarm(unavail3);

        hub.addAlarm(fibreLoss1);
        hub.addAlarm(fibreLoss2);
        hub.addAlarm(fibreLoss3);

        List<Alarm> retrivedAlarms = hub.getAlarmByType(POWER_OUTAGE);

        //Test Power Outage getAlarmByType By Type
        assertEquals(powerOutage1, retrivedAlarms.get(0));
        assertEquals(powerOutage2, retrivedAlarms.get(1));
        assertEquals(powerOutage3, retrivedAlarms.get(2));

        //Test Dark Fibre getAlarmByType By Type
        retrivedAlarms = hub.getAlarmByType(DARK_FIBRE);
        assertEquals(darkFibre1, retrivedAlarms.get(0));
        assertEquals(darkFibre2, retrivedAlarms.get(1));
        assertEquals(darkFibre3, retrivedAlarms.get(2));

        //Test Unit unavailable getAlarmByType By Type
        retrivedAlarms = hub.getAlarmByType(UNIT_UNAVAILABLE);
        assertEquals(unavail1, retrivedAlarms.get(0));
        assertEquals(unavail2, retrivedAlarms.get(1));
        assertEquals(unavail3, retrivedAlarms.get(2));

        //Test optical fibre loss getAlarmByType By Type
        retrivedAlarms = hub.getAlarmByType(OPTICAL_FIBRE_LOSS);
        assertEquals(fibreLoss1, retrivedAlarms.get(0));
        assertEquals(fibreLoss2, retrivedAlarms.get(1));
        assertEquals(fibreLoss3, retrivedAlarms.get(2));

        //Test null check
        assertNull(hub.getAlarmByType(null));

    }

    @Test
    public void getAlarmListTest() {

        Hub hub = hubBuilder.build();

        //Alarms
        Alarm powerOutage = new AlarmBuilder(1,LocalDateTime.now(), POWER_OUTAGE).build();
        Alarm fibreLoss = new AlarmBuilder(2,LocalDateTime.now(), OPTICAL_FIBRE_LOSS).build();
        Alarm unitUnavail = new AlarmBuilder(3, LocalDateTime.now(), UNIT_UNAVAILABLE).build();
        hub.addAlarm(powerOutage);
        hub.addAlarm(fibreLoss);
        hub.addAlarm(unitUnavail);

        //Actual
        List<Alarm> actual = hub.getAlarmList();

        //Test
        assertTrue(actual.contains(powerOutage));
        assertTrue(actual.contains(fibreLoss));
        assertTrue(actual.contains(unitUnavail));
        assertTrue(actual.size() == 3);


    }

    @Test
    public void getAlarmsTest() {

        Hub hub = hubBuilder.build();

        //Alarms
        Alarm powerOutage = new AlarmBuilder(1,LocalDateTime.now(), POWER_OUTAGE).build();
        Alarm fibreLoss = new AlarmBuilder(2,LocalDateTime.now(), OPTICAL_FIBRE_LOSS).build();
        Alarm unitUnavail = new AlarmBuilder(3, LocalDateTime.now(), UNIT_UNAVAILABLE).build();
        hub.addAlarm(powerOutage);
        hub.addAlarm(fibreLoss);
        hub.addAlarm(unitUnavail);

        //Actual
        Map<String, List<Alarm>> actual = hub.getAlarms();

        //Test alarms present
        assertTrue(actual.containsKey(POWER_OUTAGE.toString()));
        assertTrue(actual.containsKey(OPTICAL_FIBRE_LOSS.toString()));
        assertTrue(actual.containsKey(UNIT_UNAVAILABLE.toString()));
        assertTrue(actual.size() == 3);

        //Test alarms added are equal to what is expected
        assertTrue(powerOutage.equals(actual.get(POWER_OUTAGE.toString()).get(0)));
        assertTrue(fibreLoss.equals(actual.get(OPTICAL_FIBRE_LOSS.toString()).get(0)));
        assertTrue(unitUnavail.equals(actual.get(UNIT_UNAVAILABLE.toString()).get(0)));

    }

    @Test
    public void clearAlarmTest() {
        Alarm newAlarm = new AlarmBuilder(1,LocalDateTime.now(), POWER_OUTAGE).currentStatus(CRITICAL).build();
        Alarm dummy = new AlarmBuilder(2,LocalDateTime.now(), POWER_OUTAGE).currentStatus(MINOR).build();
        Hub hub = new HubBuilder(12,"R_001").build();

        //ensure null rejected
        boolean op1 = hub.clearAlarm(null);
        assertFalse(op1);

        //Test removal of same type but not added to net element
        hub.addAlarm(newAlarm);
        boolean op2 = hub.clearAlarm(dummy);
        assertFalse(op2);
    }

    /* Equals defined in this class can be override in subtype as
    I am not concerned about covariant comparisons. Equals
    will be reflective, symmetric and transitive with objects of the same
    class type, which for this application is fine. However these properties are
    not guaranteed and are unlikely for comparisons with covariant types i.e super type ref holding sub type
    or subtype and supertype comparisons.
    */

    @Test
    public void equalsPropertiesTest() {

        /* Reflexivity */
        Hub a = new HubBuilder(123, "H_234").ipAddress("127.0.0.1").parentId(89).description("Important Hub Description").build();
        assertTrue(a.equals(a));

        /* Symmetry */
        Hub b = new HubBuilder(123, "H_234").ipAddress("127.0.0.1").parentId(89).description("Important Hub Description").build();
        assertTrue(a.equals(b));
        assertTrue(b.equals(a));

        /* Transitivity */
        Hub c = new HubBuilder(123, "H_234").ipAddress("127.0.0.1").parentId(89).description("Important Hub Description").build();
        assertTrue(b.equals(c));
        assertTrue(a.equals(c));

        /* Consistency */
        for(int i=0; i<100; i++)
            assertTrue(a.equals(b));

        /* Non Nullity */
        assertFalse(a.equals(null));
    }

    @Test
    public void equalsTest() {

        Hub a = new HubBuilder(123, "H_234").build();
        Hub b = new HubBuilder(12, "H_234").build();
        assertFalse(a.equals(b));

        //Null being passed
        assertFalse(a.equals(null));

        //Unrelated object being passed
        assertFalse(a.equals(new Object()));

        //Name not Equals
        a = new HubBuilder(12, "H_111").build();
        b = new HubBuilder(12, "H_222").build();
        assertFalse(a.equals(b));

        //IP not equal
        a = new HubBuilder(12, "H_111").ipAddress("234.12.14.50").build();
        b = new HubBuilder(12, "H_111").ipAddress("127.0.0.0").build();
        assertFalse(a.equals(b));

        //Parent ID not equal
        a = new HubBuilder(12, "H_111").parentId(12).build();
        b =  new HubBuilder(12, "H_111").parentId(34).build();
        assertFalse(a.equals(b));

        //Description not equal
        a = new HubBuilder(12, "H_111").description("Hello").build();
        b =  new HubBuilder(12, "H_111").description("World").build();
        assertFalse(a.equals(b));

        //Different alarms
        a = new HubBuilder(12, "H_111").build();
        b = new HubBuilder(12, "H_111").build();
        Alarm powerOutage = new AlarmBuilder(1,LocalDateTime.now(), POWER_OUTAGE).build();
        Alarm unitUnavail = new AlarmBuilder(2, LocalDateTime.now(), UNIT_UNAVAILABLE).build();
        a.addAlarm(powerOutage);
        b.addAlarm(unitUnavail);
        assertFalse(a.equals(b));

    }

    @Test
    public void hashCodeTest() {
        Hub a = new HubBuilder(123, "H_234").ipAddress("127.0.0.1").parentId(89).description("Important Hub Description").build();
        Hub b = new HubBuilder(123, "H_234").ipAddress("127.0.0.1").parentId(89).description("Important Hub Description").build();

        int aHash = a.hashCode();
        int bHash = b.hashCode();
        assertEquals(aHash, bHash);
    }

}