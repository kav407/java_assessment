package err.errigal.network_elements;

import err.errigal.network_elements.Alarm.AlarmBuilder;
import err.errigal.network_elements.Hub.HubBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.swing.text.DateFormatter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static err.errigal.network_elements.AlarmType.DARK_FIBRE;
import static err.errigal.network_elements.AlarmType.POWER_OUTAGE;
import static err.errigal.network_elements.AlarmType.Status.*;
import static err.errigal.network_elements.AlarmType.UNIT_UNAVAILABLE;
import static org.junit.Assert.*;

public class AlarmTest {

    private AlarmBuilder alarmBuilder;
    private static DateTimeFormatter df;

    @BeforeClass
    public static void beforeClass() {
        df = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    }

    @Before
    public void beforeTest() {
        alarmBuilder = new AlarmBuilder(1,LocalDateTime.now(), UNIT_UNAVAILABLE).currentStatus(CRITICAL);
        Alarm alarm =alarmBuilder.build();
    }


    @Test
    public void getCreatedDateTest() {
        LocalDateTime createDate = LocalDateTime.now();
        String expected = df.format(createDate);
        Alarm alarm = new AlarmBuilder(2, createDate, UNIT_UNAVAILABLE).build();
        assertEquals(expected, df.format(alarm.getCreatedDate()));
    }

    @Test
    public void getClearDateTest() {
        LocalDateTime clearDate = LocalDateTime.now();
        String expected = df.format(clearDate);
        Alarm alarm = new AlarmBuilder(1, LocalDateTime.now(), UNIT_UNAVAILABLE).clearDate(clearDate).build();
        assertEquals(expected, df.format(alarm.getClearDate()));
    }

    @Test
    public void getAlarmTypeTest() {
        Alarm alarm = new AlarmBuilder(1, LocalDateTime.now(), POWER_OUTAGE).build();
        AlarmType expected = POWER_OUTAGE;
        assertEquals(expected, alarm.getAlarmType());
    }


    @Test
    public void getPreviousAlarmStatusTest() {
        Alarm newAlarm = new AlarmBuilder(1, LocalDateTime.now(), POWER_OUTAGE).currentStatus(CRITICAL).build();
        Hub hub = new HubBuilder(12,"R_001").build();

        //Add alarm to net element, check its prev status
        hub.addAlarm(newAlarm);
        assertEquals(null, hub.getAlarmByType(POWER_OUTAGE).get(0).getPreviousStatus());
        assertEquals(CRITICAL, hub.getAlarmByType(POWER_OUTAGE).get(0).getCurrentStatus());

        //Clear the alarm
        hub.clearAlarm(newAlarm);

        //Check prev status was correctly logged
        assertEquals(CRITICAL, hub.getAlarmByType(POWER_OUTAGE).get(0).getPreviousStatus());

    }

    @Test
    public void getCurrentAlarmStatusTest() {
        Alarm newAlarm = new AlarmBuilder(1, LocalDateTime.now(), POWER_OUTAGE).currentStatus(CRITICAL).build();
    }

    @Test
    public void toStringTest() {
        LocalDateTime time = LocalDateTime.now();
        DateTimeFormatter df =DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        Alarm current = new AlarmBuilder(1, time, UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();
        String expected = "ALARM ID: 1, ALARM TYPE: UNIT UNAVAILABLE, CREATED: "+df.format(time)+",  CLEAR_DATE: PENDING, CURRENT STATUS: CRITICAL, PREVIOUS STATUS: null";
        assertEquals(expected, current.toString());
    }

    @Test
    public void getRemedyTest() {
        Alarm current = new AlarmBuilder(1, LocalDateTime.now(), UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();
        assertEquals("TECH CALL OUT",current.getRemedy());
    }

    @Test
    public void hashCodeTest() {
        LocalDateTime createDate = LocalDateTime.now();
        Alarm current = new AlarmBuilder(1, createDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();
        Alarm next = new AlarmBuilder(1, createDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();

        assertEquals(current.hashCode(), next.hashCode());
    }


    @Test
    public void equals() {

        LocalDateTime createDate = LocalDateTime.now();
        Alarm a = new AlarmBuilder(1, createDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();
        Alarm b = new AlarmBuilder(1, createDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();
        Alarm c = new AlarmBuilder(1, createDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();

        //Reflexivity, Transitivity, Symmetry, Consistency, Non Nullity
        assertTrue(a.equals(a));
        assertTrue(a.equals(b));
        assertTrue(b.equals(c));
        assertTrue(a.equals(c));
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));

        for(int i=0; i<100; i++)
            assertTrue(a.equals(b));

        //prev status different
        Alarm d = new AlarmBuilder(4,createDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).previousStatus(null).build();
        Alarm e = new AlarmBuilder(5, createDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).previousStatus(CLEARED).build();
        boolean rr = d.equals(e);
        assertFalse(d.equals(e));
    }

    @Test
    public void testAlarmStatus() {
        assertTrue(CLEARED.getPriorityLevel() == 0);
        assertTrue(MINOR.getPriorityLevel() == 1);
        assertTrue(MEDIUM.getPriorityLevel() == 2);
        assertTrue(CRITICAL.getPriorityLevel() == 3);
    }

}