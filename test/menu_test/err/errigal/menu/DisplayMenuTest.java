package err.errigal.menu;

import err.errigal.application.NetworkApplication;
import err.errigal.network_elements.*;

import static err.errigal.menu.MenuType.*;
import static err.errigal.network_elements.Alarm.*;
import static err.errigal.network_elements.AlarmType.*;
import static err.errigal.network_elements.AlarmType.Status.*;
import static err.errigal.network_elements.Hub.HubBuilder;

import err.errigal.network_elements.Node.NodeBuilder;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class DisplayMenuTest {

    private DisplayMenu displayMenu;
    private IMenuDelegator menuDelegator;
    private NetworkApplication networkApplication;
    private static PrintStream stdOut;
    private ByteArrayOutputStream mockOut;
    private Hub hub1;
    private Hub hub2;
    private Hub hub3;
    private Node node1;
    private Node node2;
    private Node node3;
    private Carrier vodafone;
    private Carrier three;
    private Carrier o2;

    @BeforeClass
    public static void beforeClass() {

        stdOut = System.out;
    }

    @AfterClass
    public static void afterClass() {
        System.setOut(stdOut);
    }

    @Before
    public void beforeTest() {
        networkApplication = new NetworkApplication();
        menuDelegator = new NetworkMenuDelegator(networkApplication, null);
        displayMenu = new DisplayMenu(menuDelegator, networkApplication);
        vodafone = new Carrier(12, "VODAFONE");
        mockOut = new ByteArrayOutputStream();

        hub1 = new HubBuilder(1,"H_001").build();
        hub2 = new HubBuilder(2,"H_002").build();
        hub3 = new HubBuilder(2,"H_003").build();

        node1 = new NodeBuilder(3,"N_001").build();
        node2 = new NodeBuilder(4,"N_002").build();
        node3 = new NodeBuilder(5,"N_003").build();

        System.setOut(new PrintStream(mockOut));
    }

    @After
    public void after() throws IOException{
        mockOut.close();
    }

    @Test
    public void toStringTest() {
        String expected = "DISPLAY NETWORK ELEMENTS";
        String actual = displayMenu.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void displayNetworkTest() {
        Network network = networkApplication.getNetwork();
        menuDelegator.menuTransition(DISPLAY_MENU);

        String expected = "\nNo network entities of this type to display....\n\n";
        displayMenu.displayNetwork();
        assertTrue(mockOut.toString().contains(expected));

        //Clear output buffer
        mockOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(mockOut));

        network.addCarrier(vodafone);
        network.addHubToCarrier("VODAFONE",hub1);
        network.addHubToCarrier("VODAFONE",hub2);
        network.addHubToCarrier("VODAFONE",hub3);

        StringBuilder sb = new StringBuilder();
        sb.append("\n**********************************************************************\n\n");
        sb.append(vodafone.toString()+"\n\n");
        sb.append("\t->"+ hub1.toString()+"\n\n");
        sb.append("\t->"+ hub2.toString()+"\n\n");
        displayMenu.displayNetwork();

        assertEquals(mockOut.toString(), sb.toString());

    }

    @Test
    public void displayNetworkAlarms() {
        Network network = networkApplication.getNetwork();
        menuDelegator.menuTransition(DISPLAY_MENU);
        String expected = "\nNo alarms active or cleared alarms on network....\n\n";
        displayMenu.displayNetworkAlarms();
        assertTrue(mockOut.toString().contains(expected));

        //Clear output buffer
        mockOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(mockOut));

        network.addCarrier(vodafone);

        //Create alarm and add to hub
        LocalDateTime alarmCreateDate = LocalDateTime.now();
        Alarm darkFibreHub = new AlarmBuilder(1,alarmCreateDate, DARK_FIBRE).currentStatus(MEDIUM).build();
        Alarm unavailable = new AlarmBuilder(2, alarmCreateDate, UNIT_UNAVAILABLE).currentStatus(CRITICAL).build();

        network.addHubToCarrier("VODAFONE",hub1);
        network.addAlarmToNetworkElement(hub1.getID(), darkFibreHub);
        network.addAlarmToNetworkElement(hub1.getID(), unavailable);



        Map<NetworkElement, List<Alarm>> netAlarms = network.getNetworkAlarms();

        StringBuilder sb = new StringBuilder();

        sb.append("\n**********************************************************************\n\n");
        netAlarms.forEach((NetworkElement K, List<Alarm> V) -> {
            sb.append( "NET ID: "+K.getID()+" NET TYPE: "+K.getType()+", NET NAME: "+K.getName()+", NUM ALARMS: "+K.getAlarmList().size()+
                    ", STATUS: "+K.getState()+", PARENT ID: "+K.getParentID()+" PARENT NAME: "+network.elementNameFromID(K.getParentID())+"\n\n");
            V.forEach((Alarm alarm) -> sb.append("\t->"+alarm+", REMEDY: "+alarm.getRemedy()+"\n\n"));
        });

        displayMenu.displayNetworkAlarms();

        assertEquals(sb.toString(), mockOut.toString());

    }
    @Test
    public void getMenuDisplayTest() throws Exception {
        String expected =   "1. Display Entire Network      \t\t2. Display Alarm Status\n"+
                            "3. Display Network Carriers    \t\t4. Display Network Hubs\n"+
                            "5. Display Network Nodes       \t\t6. Display Hubs By Carrier\n"+
                            "7. Display Nodes By Hub        \t\t8. Exit to Main Menu\n";

        assertEquals(expected, displayMenu.getMenuDisplay());

    }

    @Test
    public void processInput() {
        menuDelegator.menuTransition(DISPLAY_MENU);                     //Change to Display Menu
        MenuType expected = DISPLAY_MENU;                                  //Expected To Change to Main Menu
        displayMenu.processInput(6);                                    //Menu option 6 on Display menu selects main menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());
    }

}