package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.network_elements.Network;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class AlarmMenuTest {

    private AlarmMenu alarmMenu;
    private static NetworkMenuDelegator menuDelegator;
    private static NetworkApplication networkApplication;
    private static Network network;

    @BeforeClass
    public static void beforeClass() {
        network = new Network();
        networkApplication = new NetworkApplication();
        menuDelegator = new NetworkMenuDelegator(networkApplication, null);
    }

    @Before
    public void beforeTest() {
        alarmMenu = new AlarmMenu(menuDelegator, networkApplication);
    }

    @Test
    public void toStringTest() {
        String expected = "ALARM CONFIG";
        assertEquals(expected, alarmMenu.toString());
    }

    @Test
    public void getMenuDisplayTest() {
        String expected =   "1. Create Alarm For Hub        \t\t2. Create Alarm For Node\n"+
                            "3. Clear Existing Alarm        \t\t4. Removed all cleared network alarms\n" +
                            "5. Exit to main menu\n";

        assertEquals(expected, alarmMenu.getMenuDisplay());
    }

    @Test
    public void processInputTest() {
        menuDelegator.menuTransition(MenuType.ALARM_MENU);              //Change to Alarm Menu
        MenuType expected = MenuType.MAIN_MENU;                         //Expected To Change to Main Menu
        alarmMenu.processInput(5);                                      //Menu option 3 on Alarm menu selects Main Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());
    }

}