package err.errigal.menu;

import err.errigal.application.NetworkApplication;
import err.errigal.network_elements.Network;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class RemoveMenuTest {

    private RemoveMenu removeMenu;
    private static NetworkMenuDelegator menuDelegator;
    private static NetworkApplication networkApplication;
    private static Network network;

    @BeforeClass
    public static void beforeClass() {
        network = new Network();
        networkApplication = new NetworkApplication();
        menuDelegator = new NetworkMenuDelegator(networkApplication, null);
    }

    @Before
    public void beforeTest() {
        removeMenu = new RemoveMenu(menuDelegator, networkApplication);
    }

    @Test
    public void toStringTest() throws Exception {
        String expected = "REMOVE NETWORK ELEMENTS";
        assertEquals(expected, removeMenu.toString());
    }

    @Test
    public void getMenuDisplayTest() {
        String expected =   "1. Remove Carrier From Network \t\t2. Remove Hub From Carrier\n"+
                            "3. Remove Node From Hub        \t\t4. Exit to Main Menu\n";
        assertEquals(expected, removeMenu.getMenuDisplay());
    }

    @Test
    public void processInputTest() throws Exception {
        menuDelegator.menuTransition(MenuType.REMOVE_MENU);              //Change to Remove Menu
        MenuType expected = MenuType.MAIN_MENU;                         //Expected To Change to err.errigal.application.Main Menu
        removeMenu.processInput(4);                                     //Menu option 4 on Remove menu selects err.errigal.application.Main Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());
    }

}