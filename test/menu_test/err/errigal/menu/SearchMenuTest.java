package err.errigal.menu;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import err.errigal.application.NetworkApplication;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.Carrier;
import err.errigal.network_elements.Hub;
import err.errigal.network_elements.Network;
import err.errigal.network_elements.Node;
import err.errigal.network_elements.Node.NodeBuilder;
import org.junit.*;

import java.io.*;

import static err.errigal.menu.MenuType.SEARCH_MENU;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;


public class SearchMenuTest {

    private static SearchMenu searchMenu;
    private NetworkMenuDelegator menuDelegator;
    private NetworkApplication networkApplication;
    private ByteArrayOutputStream out;
    private Network network;
    private static PrintStream stdOut;


    @BeforeClass
    public static void beforeClass() {

        stdOut = System.out;

    }

    @AfterClass
    public static void afterClass() {
        System.setOut(stdOut);
    }
    @Before
    public void beforeTest() {
        networkApplication = new NetworkApplication();
        menuDelegator = new NetworkMenuDelegator(networkApplication, null);
        searchMenu = new SearchMenu(menuDelegator, networkApplication);
        out = new ByteArrayOutputStream();
        network = networkApplication.getNetwork();
    }

    @After
    public void afterTest() {
        network = null;
    }


    @Test
    public void toStringTest() throws Exception {
        String expected = "NETWORK ELEMENT SEARCH";
        assertEquals(expected, searchMenu.toString());
    }

    @Test
    public void getMenuDisplayTest() throws Exception {

        String expected =   "1. Hub name search     \t\t2. Hub ID search\n"+
                            "3. Node name search    \t\t4. Node ID search\n" +
                            "5. Exit";

        menuDelegator.menuTransition(SEARCH_MENU);

        assertEquals(expected, searchMenu.getMenuDisplay());
    }


    @Test
    public void hubNameSearchTest() {
        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addCarrier(new Carrier(2,"Three"));
        network.addCarrier(new Carrier(3,"O2"));
        network.addCarrier(new Carrier(3,"Meteor"));


        network.addHubToCarrier("Vodafone", new Hub.HubBuilder(33, "H_001").build());
        network.addHubToCarrier("Three", new Hub.HubBuilder(55, "H_001").build());
        network.addHubToCarrier("O2", new Hub.HubBuilder(77, "H_001").build());
        network.addHubToCarrier("Meteor", new Hub.HubBuilder(88, "H_001").build());

        System.setOut(new PrintStream(out));

        menuDelegator.menuTransition(SEARCH_MENU);
        String mockInput = "1"+System.getProperty("line.separator")+"H_002";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        //assert hub not found
        assertTrue(out.toString().contains("Hub name not found on network......"));

        mockInput = "1"+System.getProperty("line.separator")+"H_001";
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        //assert hubs found under name
        assertTrue(out.toString().contains("*** Hub name(s) found ****"));

    }

    @Test
    public void hubIDSearch() {

        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addCarrier(new Carrier(2,"Three"));
        network.addCarrier(new Carrier(3,"O2"));
        network.addCarrier(new Carrier(4,"Meteor"));

        network.addHubToCarrier("Vodafone", new Hub.HubBuilder(33, "H_001").build());
        network.addHubToCarrier("Three", new Hub.HubBuilder(55, "H_001").build());
        network.addHubToCarrier("O2", new Hub.HubBuilder(77, "H_001").build());
        network.addHubToCarrier("Meteor", new Hub.HubBuilder(88, "H_001").build());
        network.addNodeToHub("Vodafone", "H_001", new NodeBuilder(101, "N_001").build());


        System.setOut(new PrintStream(out));

        menuDelegator.menuTransition(SEARCH_MENU);
        String mockInput = "2"+System.getProperty("line.separator")+"5666";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        //assert hub not found
        assertTrue(out.toString().contains("** Hub ID not found ***"));

        //assert that node id is rejected
        mockInput = "2"+System.getProperty("line.separator")+"101";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        //assert hub not found
        assertTrue(out.toString().contains("Sorry no hub element under this id: 101"));

        mockInput = "2"+System.getProperty("line.separator")+"33";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        assertTrue(out.toString().contains("** Hub ID found ***"));

    }

    @Test
    public void nodeNameSearchTest() {
        network.addCarrier(new Carrier(1,"Vodafone"));

        network.addHubToCarrier("Vodafone", new Hub.HubBuilder(33, "H_001").build());
        network.addNodeToHub("Vodafone", "H_001", new Node.NodeBuilder(11, "N_001").build());

        System.setOut(new PrintStream(out));

        menuDelegator.menuTransition(SEARCH_MENU);
        String mockInput = "3"+System.getProperty("line.separator")+"N_101";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        //assert node not found
        assertTrue(out.toString().contains("Node name not found on network......"));

        mockInput = "3"+System.getProperty("line.separator")+"N_001";
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        //assert hubs found under name
        assertTrue(out.toString().contains("*** Node name(s) found ****"));
    }

    @Test
    public void nodeIDSearchTest() {

        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addHubToCarrier("Vodafone", new Hub.HubBuilder(33, "H_001").build());
        network.addNodeToHub("Vodafone", "H_001", new Node.NodeBuilder(11, "N_001").build());

        InputStream stdin =  System.in;
        System.setOut(new PrintStream(out));

        menuDelegator.menuTransition(SEARCH_MENU);
        String mockInput = "4"+System.getProperty("line.separator")+"5666";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        //assert hub not found
        assertTrue(out.toString().contains("** Node ID not found ***"));

        //assert that node id is rejected
        mockInput = "4"+System.getProperty("line.separator")+"101";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        //assert hub not found
        assertTrue(out.toString().contains("** Node ID not found ***"));

        mockInput = "4"+System.getProperty("line.separator")+"11";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));

        menuDelegator.processMenuAction();

        assertTrue(out.toString().contains("** Node ID found ***"));
    }
    @Test
    public void processInputTest() throws Exception {
        menuDelegator.menuTransition(SEARCH_MENU);                          //Change to Search Menu
        MenuType expected = MenuType.MAIN_MENU;                             //Expected To Change to Main Menu
        searchMenu.processInput(5);                                         //Menu option 4 on Search menu selects Main Menu
        Assert.assertEquals(expected, menuDelegator.getCurrentMenuType());
    }
}