package err.errigal.menu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import err.errigal.io.Adaptor;
import err.errigal.io.InputHandler;
import err.errigal.application.NetworkApplication;
import err.errigal.network_elements.Network;
import err.errigal.network_elements.NetworkElement;
import err.errigal.patternInterfaces.Observable;
import err.errigal.patternInterfaces.Observer;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


import static org.junit.Assert.*;

public class MainMenuTest {

    private static NetworkApplication networkApplication;
    private static Network network;
    private static InputStream mockUserInput;
    private static IMenuDelegator menuDelegator;
    private MainMenu mainMenu;


    @BeforeClass
    public static void beforeClass() throws Exception {
        network = new Network();
        GsonBuilder builder = new GsonBuilder().registerTypeAdapter(NetworkElement.class, new Adaptor());
        builder.registerTypeAdapter(Observer.class, new Adaptor());
        Gson gson = builder.registerTypeAdapter(Observable.class, new Adaptor()).setPrettyPrinting().create();
        networkApplication = new NetworkApplication();
        menuDelegator = new NetworkMenuDelegator(networkApplication, gson);
    }

    @Before
    public void beforeTest() {
        GsonBuilder builder = new GsonBuilder().registerTypeAdapter(NetworkElement.class, new Adaptor());
        builder.registerTypeAdapter(Observer.class, new Adaptor());
        Gson gson = builder.registerTypeAdapter(Observable.class, new Adaptor()).setPrettyPrinting().create();
        mainMenu = new MainMenu(menuDelegator, networkApplication, gson);
    }
    @Test
    public void toStringTest() throws Exception {

        String expected = "MAIN MENU";
        String actual = mainMenu.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void getMenuTitleTest() throws Exception {

        String expected =   "**********************************************************************\n"+
                            "            "+mainMenu+"             \n"+
                            "**********************************************************************\n";

        String actual = mainMenu.getMenuTitle();
        assertEquals(expected, actual);
    }

    @Test
    public void getMenuTest() throws Exception {

        String expected =   "1. Display Elements Menu           \t\t2. Display Element(s) Status\n"+
                            "3. Add Network Element             \t\t4. Remove Network Element\n"+
                            "5. Network Element Search          \t\t6. Edit Network Elements\n"+
                            "7. Network Element Alarm Config    \t\t8. Export Network To JSON\n"+
                            "9. Import Network From JSON        \t\t10. Exit\n";

        String actual = mainMenu.getMenuDisplay();
        assertEquals(expected, actual);
    }

    /* Test main menu options are valid and accept correct range range*/
    @Test
    public void handleMenuActionTest() {

        for(int i=1;i<mainMenu.MAX_MENU_OPTION;i++) {
            String menuOp = Integer.toString(i);
            mockUserInput = new ByteArrayInputStream(menuOp.getBytes());
            InputHandler.setInputStream(mockUserInput);
            if(i%8==0 || i%9==0) continue;
            boolean expected = true;
            assertEquals(expected, mainMenu.handleMenuAction());
        }

        //option 7
        mockUserInput = new ByteArrayInputStream(("8"+System.getProperty("line.separator")+"src/network.json").getBytes());
        InputHandler.setInputStream(mockUserInput);
        assertEquals(true, mainMenu.handleMenuAction());

        //option 8
        mockUserInput = new ByteArrayInputStream(("9"+System.getProperty("line.separator")+"src/network.json").getBytes());
        InputHandler.setInputStream(mockUserInput);
        assertEquals(true, mainMenu.handleMenuAction());
    }

    /* Test main menu options are valid and accept correct range range*/
    @Test
    public void handleMenuActionInvalidTest() {

        //Mock user input for character/string input when int expected
        mockUserInput = new ByteArrayInputStream("sfdfsf".getBytes());
        InputHandler.setInputStream(mockUserInput);
        boolean expected = false;
        assertEquals(expected, mainMenu.handleMenuAction());

        //Mock user input for out of range option negative
        mockUserInput = new ByteArrayInputStream("-23".getBytes());
        InputHandler.setInputStream(mockUserInput);
        assertEquals(expected, mainMenu.handleMenuAction());

        //Mock user input for out of range option positive
        mockUserInput = new ByteArrayInputStream("453".getBytes());
        InputHandler.setInputStream(mockUserInput);
        assertEquals(expected, mainMenu.handleMenuAction());

        //Mock user input for out of range upper boundary
        mockUserInput = new ByteArrayInputStream("11".getBytes());
        InputHandler.setInputStream(mockUserInput);
        assertEquals(expected, mainMenu.handleMenuAction());

        //Mock user input for out of range lower boundary
        mockUserInput = new ByteArrayInputStream("0".getBytes());
        InputHandler.setInputStream(mockUserInput);
        assertEquals(expected, mainMenu.handleMenuAction());



    }

    //Test menu transitions from main menu to other menu's
    @Test
    public void processUserInputTest() {

        //Test Menu Option 1
        MenuType expected = MenuType.DISPLAY_MENU;                      //Expected To Change to Display Menu
        mainMenu.processInput(1);                                       //Menu option 1 on Main menu selects Display Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());

        //Test Menu Option 2
        menuDelegator.menuTransition(MenuType.MAIN_MENU);               //Change back to Main Menu
        expected = MenuType.STATUS_MENU;                                //Expected To Change to Status Menu From Main Menu
        mainMenu.processInput(2);                                       //Main Menu option 4 on Main menu selects Add Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());

        //Test Menu Option 3
        menuDelegator.menuTransition(MenuType.MAIN_MENU);               //Change back to Main Menu
        expected = MenuType.ADD_MENU;                                   //Expected To Change to Remove Menu From err.errigal.application.Main Menu
        mainMenu.processInput(3);                                       //Main Menu option 3 on Main menu selects Remove Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());

        //Test Menu Option 4
        menuDelegator.menuTransition(MenuType.MAIN_MENU);               //Change back to Main Menu
        expected = MenuType.REMOVE_MENU;                                //Expected To Change to Search Menu From Main Menu
        mainMenu.processInput(4);                                       //Main Menu option 4 on Main menu selects Search Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());

        //Test Menu Option 5
        menuDelegator.menuTransition(MenuType.MAIN_MENU);               //Change back to Main Menu
        expected = MenuType.SEARCH_MENU;                                //Expected To Change to Edit Menu From Main Menu
        mainMenu.processInput(5);                                       //Main Menu option 5 on Main menu selects Edit Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());

        //Test Menu Option 6
        menuDelegator.menuTransition(MenuType.MAIN_MENU);               //Change back to Main Menu
        expected = MenuType.EDIT_MENU;                                  //Expected To Change to Alarm Menu From Main Menu
        mainMenu.processInput(6);                                       //Main Menu option 6 on Main menu selects Alarm Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());

        //Test Menu Option 7
        menuDelegator.menuTransition(MenuType.MAIN_MENU);               //Change back to Main Menu
        expected = MenuType.ALARM_MENU;                                 //Expected To Change to Alarm Menu From Main Menu
        mainMenu.processInput(7);                                       //Main Menu option 7 on Main menu selects Alarm Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());

        //Exit Option
        menuDelegator.menuTransition(MenuType.MAIN_MENU);                                   //Change back to err.errigal.application.Main Menu
        NetworkApplication.NetworkAppState result = NetworkApplication.NetworkAppState.INACTIVE;    //Expected close application
        mainMenu.processInput(10);                                                           //Main Menu option 8 to exit
        assertEquals(result, networkApplication.getState());
    }

    @Test
    public void getMenuTypeTest() {
        assertEquals(MenuType.MAIN_MENU, mainMenu.getMenuType());
    }

}