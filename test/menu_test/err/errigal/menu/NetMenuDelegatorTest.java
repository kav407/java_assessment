package err.errigal.menu;

import err.errigal.application.NetworkApplication;
import err.errigal.network_elements.Network;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class NetMenuDelegatorTest {

    private NetworkMenuDelegator menuManager;
    private static NetworkApplication networkApplication;
    private static Network network;

    @BeforeClass
    public static void beforeClass() {
        network = new Network();
        networkApplication = new NetworkApplication();
    }

    @Before
    public void setUp() {
        menuManager = new NetworkMenuDelegator(networkApplication, null);
    }

    @After
    public void afterTest() {
    }

    @Test
    public void menuTransitionTest() {
        //Check main menu transition
        MenuType expected = MenuType.MAIN_MENU;
        menuManager.menuTransition(MenuType.MAIN_MENU);
        assertEquals(expected, menuManager.getCurrentMenuType());


        //Check display menu transition
        expected = MenuType.DISPLAY_MENU;
        menuManager.menuTransition(MenuType.DISPLAY_MENU);
        assertEquals(expected, menuManager.getCurrentMenuType());

        //Check remove menu transition
        expected = MenuType.REMOVE_MENU;
        menuManager.menuTransition(MenuType.REMOVE_MENU);
        assertEquals(expected, menuManager.getCurrentMenuType());

        //Check add menu transition
        expected = MenuType.ADD_MENU;
        menuManager.menuTransition(MenuType.ADD_MENU);
        assertEquals(expected, menuManager.getCurrentMenuType());

        //Check alarm menu transition
        expected = MenuType.ALARM_MENU;
        menuManager.menuTransition(MenuType.ALARM_MENU);
        assertEquals(expected, menuManager.getCurrentMenuType());

        //Check edit menu transition
        expected = MenuType.EDIT_MENU;
        menuManager.menuTransition(MenuType.EDIT_MENU);
        assertEquals(expected, menuManager.getCurrentMenuType());

        //Check edit menu transition
        NetworkApplication.NetworkAppState result  = NetworkApplication.NetworkAppState.INACTIVE;
        menuManager.menuTransition(MenuType.EXIT);
        assertEquals(result, networkApplication.getState());

    }

    @Test
    public void getCurrentMenuTypeTest() {
        MenuType expected = MenuType.MAIN_MENU;
        menuManager.menuTransition(MenuType.MAIN_MENU);
        MenuType actual = menuManager.getCurrentMenuType();
        assertEquals(expected, actual);
    }

}