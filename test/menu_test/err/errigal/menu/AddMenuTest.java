package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.network_elements.Network;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class AddMenuTest {

    private AddMenu addMenu;
    private static NetworkMenuDelegator menuDelegator;
    private static NetworkApplication networkApplication;
    private static Network network;


    @BeforeClass
    public static void beforeClass() {
        networkApplication = new NetworkApplication();
        network = new Network();
        menuDelegator = new NetworkMenuDelegator(networkApplication, null);
    }

    @Before
    public void beforeTest() {
        addMenu = new AddMenu(menuDelegator, networkApplication);
    }

    @Test
    public void toStringTest() {
        String expected = "ADD NETWORK ELEMENT";
        assertEquals(expected, addMenu.toString());
    }


    @Test
    public void getMenuDisplayTest() {
        String expected =   "1. Add Carrier to Network      \t\t2. Add Hub to Carrier\n"+
                            "3. Add Node to Hub             \t\t4. Exit to Main Menu\n";
        assertEquals(expected, addMenu.getMenuDisplay());
    }

    @Test
    public void processInputTest() {
        menuDelegator.menuTransition(MenuType.ADD_MENU);            //Change to Add Menu
        MenuType expected = MenuType.MAIN_MENU;                     //Expected To Change to err.errigal.application.Main Menu
        addMenu.processInput(4);                                    //Menu option 4 on Add menu selects err.errigal.application.Main Menu
        assertEquals(expected, menuDelegator.getCurrentMenuType());
    }

    @Test
    public void addCarrier() {

    }


}