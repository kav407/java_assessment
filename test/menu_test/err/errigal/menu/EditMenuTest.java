package err.errigal.menu;
import err.errigal.application.NetworkApplication;
import err.errigal.io.InputHandler;
import err.errigal.network_elements.Carrier;
import err.errigal.network_elements.Hub;
import err.errigal.network_elements.Hub.HubBuilder;
import err.errigal.network_elements.Network;
import err.errigal.network_elements.Node;
import err.errigal.network_elements.Node.NodeBuilder;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

import static err.errigal.menu.MenuType.*;
import static org.junit.Assert.*;

public class EditMenuTest {

    private EditMenu editMenu;
    private static NetworkMenuDelegator menuDelegator;
    private static NetworkApplication networkApplication;
    private static Network network;



    @Before
    public void beforeTest() {
        editMenu = new EditMenu(menuDelegator, networkApplication);
        networkApplication = new NetworkApplication();
        menuDelegator = new NetworkMenuDelegator(networkApplication, null);
    }

    @Test
    public void toStringTest() {
        String expected = "EDIT NETWORK ELEMENTS";
        assertEquals(expected, editMenu.toString());
    }


    @Test
    public void editCarrierNameSuccess() {
        network = networkApplication.getNetwork();
        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addCarrier(new Carrier(2,"Three"));
        menuDelegator.menuTransition(EDIT_MENU);

        InputStream stdin =  System.in;
        String dt = System.getProperty("line.separator");
        String mockInput = "1"+dt+"Vodafone"+dt+"O2";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        List<Carrier> netHubs = network.getNetworkCarriers();
        assertEquals("O2", netHubs.get(1).getName()); //Name is immutable so new carrier created with new name, hence index 1
        assertEquals("Three", netHubs.get(0).getName());
    }

    @Test
    public void editCarrierNameFail() {
        network = networkApplication.getNetwork();
        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addCarrier(new Carrier(2,"Three"));
        menuDelegator.menuTransition(EDIT_MENU);

        InputStream stdin =  System.in;
        String dt = System.getProperty("line.separator");
        String mockInput = "1"+dt+"Vodafone"+dt+"Three"+dt+"-1";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        List<Carrier> netHubs = network.getNetworkCarriers();
        assertEquals("Vodafone", netHubs.get(0).getName()); //no change
        assertEquals("Three", netHubs.get(1).getName());   //no change
    }

    @Test
    public void editHubNameSuccess() {
        network = networkApplication.getNetwork();
        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addHubToCarrier("Vodafone", new HubBuilder(33, "H_001").build());
        network.addHubToCarrier("Vodafone", new HubBuilder(34, "H_002").build());
        menuDelegator.menuTransition(EDIT_MENU);

        InputStream stdin =  System.in;
        String dt = System.getProperty("line.separator");
        String mockInput = "2"+dt+"Vodafone"+dt+"1"+dt+"newHubName";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        List<Hub> netHubs = network.getHubsByName();
        assertEquals("newHubName", netHubs.get(1).getName()); //Name is immutable so new hub created with new name, hence index 1
        assertEquals("H_002", netHubs.get(0).getName()); //Name is immutable so new hub created with new name, hence index 1

    }

    @Test
    public void editHubNameFail() {
        network = networkApplication.getNetwork();
        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addHubToCarrier("Vodafone", new HubBuilder(33, "H_001").build());
        network.addHubToCarrier("Vodafone", new HubBuilder(34, "H_002").build());
        menuDelegator.menuTransition(EDIT_MENU);

        InputStream stdin =  System.in;
        String dt = System.getProperty("line.separator");
        String mockInput = "2"+dt+"Vodafone"+dt+"1"+dt+"H_002"+dt+"-1";  //Try to change hub H_001 to H_002, should fail, -1 to exit change
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        List<Hub> netHubs = network.getHubsByName();
        assertEquals("H_001", netHubs.get(0).getName());
        assertEquals("H_002", netHubs.get(1).getName());

    }

    @Test
    public void editNodeNameSuccess() {
        network = networkApplication.getNetwork();
        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addHubToCarrier("Vodafone", new HubBuilder(33, "H_001").build());
        network.addNodeToHub("Vodafone", "H_001", new NodeBuilder(66,"N_001").build());
        network.addNodeToHub("Vodafone", "H_001", new NodeBuilder(78,"N_002").build());
        menuDelegator.menuTransition(EDIT_MENU);

        InputStream stdin =  System.in;
        String dt = System.getProperty("line.separator");
        String mockInput = "3"+dt+"Vodafone"+dt+"1"+dt+"1"+dt+"NewNodeName";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        List<Node> netNodes = network.getNetworkNodes();
        assertEquals("NewNodeName", netNodes.get(1).getName()); //Name is immutable so new node created with new name, hence index 1
        assertEquals("N_002", netNodes.get(0).getName());

    }

    @Test
    public void editNodeNameFail() {
        network = networkApplication.getNetwork();
        network.addCarrier(new Carrier(1,"Vodafone"));
        network.addHubToCarrier("Vodafone", new HubBuilder(33, "H_001").build());
        network.addNodeToHub("Vodafone", "H_001", new NodeBuilder(66,"N_001").build());
        network.addNodeToHub("Vodafone", "H_001", new NodeBuilder(78,"N_002").build());
        menuDelegator.menuTransition(EDIT_MENU);

        InputStream stdin =  System.in;
        String dt = System.getProperty("line.separator");
        String mockInput = "3"+dt+"Vodafone"+dt+"1"+dt+"1"+dt+"N_002"+dt+"-1";
        InputHandler.setInputStream(new ByteArrayInputStream(mockInput.getBytes()));
        menuDelegator.processMenuAction();

        List<Node> netNodes = network.getNetworkNodes();
        assertEquals("N_001", netNodes.get(0).getName()); //Name is immutable so new node created with new name, hence index 1
        assertEquals("N_002", netNodes.get(1).getName());

    }

    @Test
    public void getMenuDisplayTest() {
        String expected =   "1. Edit Carrier Name   \t\t2. Edit Hub Name\n"+
                            "3. Edit Node Name      \t\t4. Exit to Main Menu\n";

        assertEquals(expected, editMenu.getMenuDisplay());
    }

    @Test
    public void processInputTest() {
        menuDelegator.menuTransition(EDIT_MENU);                        //Change to Edit Menu
        MenuType expected = MAIN_MENU;                                  //Expected To Change to Main Menu
        InputHandler.setInputStream(new ByteArrayInputStream("4".getBytes()));
        menuDelegator.processMenuAction();
        assertEquals(expected, menuDelegator.getCurrentMenuType());
    }

}